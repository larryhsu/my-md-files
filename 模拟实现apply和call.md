# 模拟实现apply和call

## apply

### 说明

>`apply()` 方法调用一个具有给定this值的函数，以及作为一个数组（或类似数组对象）提供的参数。

### 语法

```javascript
func.apply(thisArg[，argsArray])
```

### 参数

* `thisArg`：可选的。在 `function` 函数运行时使用的 `this` 值。请注意，`this` 可能不是该方法看到的实际值：如果这个函数处于非严格模式下，则指定为 `null` 或 `undefined` 时会自动替换为指向全局对象，原始值会被包装。

* `argsArray`：可选的。一个数组或者类数组对象，其中的数组元素将作为单独的参数传给 `func` 函数。如果该参数的值为 `null` 或`undefined`，则表示不需要传入任何参数。从 ECMAScript5 开始可以使用类数组对象。

### 返回值

* 调用有指定this值和参数的函数的结果。

### ES5 中的 apply 规范

当以 `thisArg` 和 `argArray` 为参数在一个 `func` 对象上调用 `apply` 方法，采用如下步骤：

* 如果 `IsCallable(func)` 是 `false`, 则抛出一个 `TypeError` 异常。
* 如果 `argArray` 是 `null` 或 `undefined`, 则返回提供 `thisArg` 作为 `this` 值并以空参数列表调用 `func` 的 `[[Call]]` 内部方法的结果。
* 如果 `Type(argArray)` 不是 `Object`, 则抛出一个 `TypeError` 异常。
* 提供 `thisArg` 作为 `this` 值并以 `argList` 作为参数列表，调用 `func` 的 `[[Call]]` 内部方法，返回结果。

`apply` 方法的 `length` 属性是 `2`。

> 注：在外面传入的 `thisArg` 值会修改并成为 `this` 值。
`thisArg` 是 `undefined` 或 `null` 时它会被替换成全局对象，所有其他值会被应用 `ToObject` 并将结果作为 `this` 值。

### 实现

```javascript
const isNullOrUndefined = function (obj) {
  return obj === null || typeof obj === 'undefined'
}

const isFunction = function (obj) {
  return typeof obj === 'function'
}

const getGlobalObject = function () {
  return this
}

// 使用的形式为：func.applyFn(context, argsArray)
Function.prototype.applyFn = function (thisArg, argsArray) {
  // 如果 `IsCallable(func)` 是 `false`, 则抛出一个 `TypeError` 异常。
  if (!isFunction(this)) {
    throw new TypeError(`${this} is not a function`)
  }

  // 如果 `argArray` 是 `null` 或 `undefined`, 则返回提供 `thisArg`
  // 作为 `this` 值并以空参数列表调用 `func` 的 `[[Call]]` 内部方法的结果。
  if (!argsArray) {
    argsArray = []
  } else {
    // 如果 `Type(argArray)` 不是 `Object`, 则抛出一个 `TypeError` 异常。
    if (typeof argsArray !== 'object') {
      throw new TypeError(`CreateListFromArrayLike called on non-object`)
    }
  }

  // thisArg 是 undefined 或 null 时它会被替换成全局对象
  if (isNullOrUndefined(thisArg)) {
    thisArg = getGlobalObject()
  }

  // 提供 `thisArg` 作为 `this` 值并以 `argList` 作为参数列表，
  // 调用 `func` 的 `[[Call]]` 内部方法，返回结果。
  var fn = Symbol()
  thisArg = Object(thisArg)
  thisArg[fn] = this

  var result = thisArg[fn](...argsArray)
  delete thisArg[fn]

  return result
}
```

## call

### 说明

> `call()` 方法使用一个指定的 `this` 值和单独给出的一个或多个参数来调用一个函数。

该方法的语法和作用与 `apply()` 方法类似，只有一个区别，就是 `call()` 方法接受的是一个参数列表，而 `apply()` 方法接受的是一个包含多个参数的数组。

### 语法

```javascript
function.call(thisArg, arg1, arg2, ...)
```

### 参数

* thisArg 可选的。在 function 函数运行时使用的 this 值。请注意，this可能不是该方法看到的实际值：如果这个函数处于非严格模式下，则指定为 null 或 undefined 时会自动替换为指向全局对象，原始值会被包装。

* arg1, arg2, ... 指定的参数列表。

### 返回值

* 使用调用者提供的 this 值和参数调用该函数的返回值。若该方法没有返回值，则返回 undefined。

### ES5 中的 call 规范

当以 thisArg 和可选的 arg1, arg2 等等作为参数在一个 func 对象上调用 call 方法，采用如下步骤：

* 如果 IsCallable(func) 是 false, 则抛出一个 TypeError 异常。

* 令 argList 为一个空列表。

* 如果调用这个方法的参数多余一个，则从 arg1 开始以从左到右的顺序将每个参数插入为 argList 的最后一个元素。

* 提供 thisArg 作为 this 值并以 argList 作为参数列表，调用 func 的 [[Call]] 内部方法，返回结果。

call 方法的 length 属性是 1。

> 注：在外面传入的 thisArg 值会修改并成为 this 值。thisArg 是 undefined 或 null 时它会被替换成全局对象，所有其他值会被应用 ToObject 并将结果作为 this 值，这是第三版引入的更改。

### 实现

```javascript
const isNullOrUndefined = function (obj) {
  return obj === null || typeof obj === 'undefined'
}

const isFunction = function (obj) {
  return typeof obj === 'function'
}

const getGlobalObject = function () {
  return this
}

Function.prototype.callFn = function (thisArg) {
  // 如果 IsCallable(func) 是 false, 则抛出一个 TypeError 异常。
  if (!isFunction(this)) {
    throw new TypeError(`${this} is not a function`)
  }

  // 令 argList 为一个空列表。
  var argList = []

  // 如果调用这个方法的参数多余一个，则从 arg1 开始以从左到右的顺序将每个参数插
  // 入为 argList 的最后一个元素。
  for (let i=1; i<arguments.length; i++) {
    argList.push(arguments[i])
  }

  // 提供 thisArg 作为 this 值并以 argList 作为参数列表，调用 func 的 [[Call]] 内部方法，
  // 返回结果。thisArg 是 undefined 或 null 时它会被替换成全局对象，
  // 所有其他值会被应用 ToObject 并将结果作为 this 值
  if (isNullOrUndefined(thisArg)) {
    thisArg = getGlobalObject()
  }

  var fn = Symbol()
  thisArg = Object(thisArg)
  thisArg[fn] = this

  var result = thisArg[fn](...argList)
  delete thisArg[fn]

  return result
}
```
