## 规范

promise有多种规范，像 promise/A、 promise/B、 promise/D 以及 promise/A 的升级版 promise/A+。
ES6中采用的规范是promise/A+。

## 状态

一个promise的状态只有三种：等待态Pending、执行态Fulfilled和拒绝态Rejected。

处于等待态时，promise可以迁移到执行态或者拒绝态。
处于执行态时，promise不能迁移到其他任何状态，且必须拥有一个不可变的终值。
处于拒绝态时，promise不能迁移到其他任何状态，且必须拥有一个不可变的据因。

## 实现

### 构造函数

```javascript
function Promise(executor) {
  // init
  this.state = 'pending'
  // 成功后必须有一个终值
  this.value = ''
  // 失败后必须有一个据因
  this.reason = ''
  // 成功时的回调函数集
  this.onFulfilledSucc = []
  // 失败的回调函数集
  this.onRejectedFail = []

  var resolve = value => {
    if (this.state === 'pending') {
      this.state = 'fulfilled'
      this.value = value;
      // 执行回调函数集中的函数
      this.onFulfilledSucc.forEach(fn => fn())
    }
  }

  var reject = reason => {
    if (this.state === 'pending') {
      this.state = 'rejected'
      this.reason = reason;
      // 执行回调函数集中的函数
      this.onRejectedFail.forEach(fn => fn())
    }
  }

  // executor执行发生错，则直接reject
  try {
    executor(resolve, reject)
  } catch (e) {
    reject(e)
  }
}
```

### catch原型方法

Promise.prototype.catch方法是.then(null, rejection)或.then(undefined, rejection)的别名，用于指定发生错误时的回调函数。所以很简单就可以实现了。

```javascript
Promise.prototype.catch = function (fn) {
  return this.then(null, fn);
}
```

### then原型方法

一个promise必须提供一个then方法可以访问其当前值、终值和据因。promise的then方法接受两个参数：

```javascript
promise.then(onFulfilled, onRejected)
```

onFulfilled和onRejected都是可选的参数:

* 如果onFulfilled不是函数，其必须被忽略
* 如果onRejected不是函数，其必须被忽略

且then方法必须返回一个promise对象：

```javascript
promise2 = promise1.then(onFulfilled, onRejected)
```

且onFulfilled和onRejected不能同步调用，必须异步调用，下面是代码：

```javascript
Promise.prototype.then = function(onFulfilled, onRejected){
  // 如果onFulfilled不是函数，其必须被忽略
  if (typeof onFulfilled !== 'function') {
    onFulfilled = value => value
  }

  // 如果onRejected不是函数，其必须被忽略
  if (typeof onRejected !== 'function') {
    onRejected = err => {throw err}
  }
  var promise2 = new Promise((resolve, reject) => {
    if (this.state === 'fulfilled') {
      // 规定onFulfilled或onRejected不能同步被调用，必须异步调用
      setTimeout (() => {
        try {
          // 返回值很重要，下面会讲到
          var x = onFulfilled(this.value)
        } catch (e) {
          // promise2的状态改变了
          reject(e)
        }
      }, 0)
    }

    if (this.state === 'rejected') {
      setTimeout(() => {
        try {
          var x = onRejected(this.reason)
        } catch (e) {
          reject(e)
        }
      }, 0)
    }

    // 注1
    if (this.state === 'pending') {
      // onFulfilled传入到成功时的回调函数集
      this.onFulfilledSucc.push(() => {
        setTimeout(() => {
          try {
          var x = onFulfilled(this.value)
        } catch (e) {
          reject(e)
        }
        }, 0)
      })
      // onRejectedFail传入到失败时的回调函数集
      this.onRejectedFail.push(() => {
        setTimeout(() => {
          try {
            var x = onRejected(this.reason)
          } catch (e) {
            reject(e)
          }
        }, 0)
      })
    }
  })

  return promise2
}
```

>注1：当resolve在setTomeout内执行，then时state还是pending等待状态 我们就需要在then调用的时候，将成功和失败存到各自的数组，一旦reject或者resolve，就调用它们

### 链式调用

在使用promise的时候经常用到new Promise().then().then()的写法，这个就是链式调用了。

之前的then方法已经默认返回了一个promise，但是还没有完全实现链式调用的写法。还需要将这个promise2返回的值传递到下一个then中去。

如果在第一个then方法中return了一个值，这个值就称为x。需要对x进行判断，从而根据x的值来改变promise2的状态，而判断x的函数叫做resolvePromise。

* 如果 onFulfilled 或者 onRejected 返回一个值 x ，则运行下面的 Promise 解决过程：[[Resolve]](promise2, x)
* 如果 onFulfilled 或者 onRejected 抛出一个异常 e ，则 promise2 必须拒绝执行，并返回拒因 e
* 如果 onFulfilled 不是函数且 promise1 成功执行， promise2 必须成功执行并返回相同的值
* 如果 onRejected 不是函数且 promise1 拒绝执行， promise2 必须拒绝执行并返回相同的据因

如果 x 为对象或者函数：

* 把 x.then 赋值给 then
* 如果取 x.then 的值时抛出错误 e ，则以 e 为据因拒绝 promise
* 如果 then 是函数，将 x 作为函数的作用域 this 调用之。后面是成功的回调和失败的回调
* 如果成功的回调还是pormise，就递归继续解析

如果 x 不为对象或者函数，以 x 为参数执行 promise

```javascript
function resolvePromise(promise2, x, resolve, reject) {
  // 如果 x === promise2，则会造成循环引用
  if (x === promise2) {
    return reject(new TypeError('Chaining cycle detected for promise!'))
  }

  // 防止多次调用
  var called

  // x不是null 且x是对象或者函数
  if (x != null && (typeof x === 'object') ||
    (typeof x === 'function')) {
    try {
      // 把 x.then 赋值给 then
      var then = x.then

      // 如果then是函数，就默认是x是promise了
      if (typeof then === 'function') {
        // 如果 then 是函数，将 x 作为函数的作用域 this 运行这个then函数。
        then.call(x, y => {
          if (called) return
          called = true
          // 如果成功的回调还是pormise，就递归继续解析
          resolvePromise(promise2, y, resolve, reject)
        }, r => {
          if (called) return
          // 如果 rejectPromise 以据因 r 为参数被调用，则以据因 r 拒绝 promise
          reject(r)
        })
      } else {
        // x不是promise时
        resolve(x)
      }
    } catch (e) {
      // 如果取 x.then 的值时抛出错误 e ，则以 e 为据因拒绝 promise
      if (called) return
      called = true
      reject(e)
    }
  }
  // 如果 x 不为对象或者函数，以 x 为参数执行 promise
  else {
    resolve(x);
  }
}
```

之后可以将then方法修改如下：

```javascript
Promise.prototype.then = function (onFulfilled, onRejected) {
  // ...省略的代码
  var promise2 = new Promise ((resolve, reject) => {
    if (this.state === 'fulfilled') {
      // 规定onFulfilled或onRejected不能同步被调用，必须异步调用
      setTimeout (() => {
        try {
          // 返回值很重要，下面会讲到
          var x = onFulfilled(this.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          // promise2的状态改变了
          reject(e)
        }
      }, 0)
    }

    if (this.state === 'rejected') {
      setTimeout(() => {
        try {
          var x = onRejected(this.reason)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
      }, 0)
    }

    // 注1
    if (this.state === 'pending') {
      // onFulfilled传入到成功时的回调函数集
      this.onFulfilledSucc.push(() => {
        setTimeout(() => {
          try {
          var x = onFulfilled(this.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
        }, 0)
      })
      // onRejectedFail传入到失败时的回调函数集
      this.onRejectedFail.push(() => {
        setTimeout(() => {
          try {
            var x = onRejected(this.reason)
            resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        }, 0)
      })
    }
  })
  
  return promise2
}
```

### finally原型方法

finally 方法不管成功还是失败都会执行，并不是最后执行的意思。并且finally是一个关键字,在IE低版本会引发解析错误，若兼容IE不能直接object.key语法。

```javascript
Promise.prototype['finally'] = function (cb) {
  var P = this.constructor
  return this.then(
    val => P.resolve(cb()).then(() => val),
    rea => P.resolve(cb()).then(() => { throw rea })
  )
}
```

### resolve方法

```javascript
Promise.resolve = function (value) {
  return new Promise((resolve, reject) => {
    resolve(value)
  })
}
```

### reject方法

```javascript
Promise.reject = function (reason) {
  return new Promise((resolve, reject) => {
    reject(reason)
  })
}
```

### all方法

```javascript
// 全部成功才算成功
Promise.all = function (arr) {
  return new Promise((resolve, reject) => {
    var cnt = 0
    var resolved = []

    for(let i=0; i<arr.length; i++) {
      Promise.resolve(arr[i]).then(
        value => {
          cnt++
          resolved[i] = value
          if (cnt === arr.length) {
            resolve(resolved)
          }
        },  
        reason => reject(reason)
      )
    }
  })
}
```

### race方法

```javascript
// 最先成功的
Promise.race = function (arr) {
  return new Promise((resolve, reject) => {
    for (let i=0; i<arr.length; i++) {
      // 如果不是 Promise 实例，先调用Promise.resolve()方法，将参数转为 Promise 实例
      Promise.resolve(arr[i]).then(
        value=>resolve(value),
        reason => reject(reason)
      )
    }
  })
}
```

### some方法

```javascript
// 全部失败才算失败
Promise.some = function (arr) {
  return new Promise((resolve, reject) => {
    var cnt = 0
    var rejected = []

    for (let i=0; i<arr.length; i++) {
      Promise.resolve(arr[i]).then(
        value => resovle(value),
        reason => {
          cnt++
          rejected[i] = reason
          if (cnt === arr.length) {
            reject(rejected)
          }
        }
      )
    }
  })
}
```

## 完整代码

<details>

<summary>点此展开</summary>

```javascript
const Promise = (function () {
  function Promise(executor) {
    // init
    this.state = 'pending'
    // 成功后必须有一个终值
    this.value = ''
    // 失败后必须有一个据因
    this.reason = ''
    // 成功时的回调函数集
    this.onFulfilledSucc = []
    // 失败的回调函数集
    this.onRejectedFail = []

    var resolve = value => {
      if (this.state === 'pending') {
        this.state = 'fulfilled'
        this.value = value;
        // 执行回调函数集中的函数
        this.onFulfilledSucc.forEach(fn => fn())
      }
    }

    var reject = reason => {
      if (this.state === 'pending') {
        this.state = 'rejected'
        this.reason = reason;
        // 执行回调函数集中的函数
        this.onRejectedFail.forEach(fn => fn())
      }
    }

    // executor执行发生错，则直接reject
    try {
      executor(resolve, reject)
    } catch (e) {
      reject(e)
    }
  }

  Promise.prototype.then = function (onFulfilled, onRejected) {
    // 如果onFulfilled不是函数，其必须被忽略
    if (typeof onFulfilled !== 'function') {
      onFulfilled = value => value
    }

    // 如果onRejected不是函数，其必须被忽略
    if (typeof onRejected !== 'function') {
      onRejected = err => {throw err}
    }

    var promise2 = new Promise((resolve, reject) => {
      if (this.state === 'fulfilled') {
        // 规定onFulfilled或onRejected不能同步被调用，必须异步调用
        setTimeout(() => {
          try {
            // 返回值很重要，下面会讲到
            var x = onFulfilled(this.value)
            resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            // promise2的状态改变了
            reject(e)
          }
        }, 0)
      }

      if (this.state === 'rejected') {
        setTimeout(() => {
          try {
            var x = onRejected(this.reason)
            resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        }, 0)
      }

      // 注1
      if (this.state === 'pending') {
        // onFulfilled传入到成功时的回调函数集
        this.onFulfilledSucc.push(() => {
          setTimeout(() => {
            try {
              var x = onFulfilled(this.value)
              resolvePromise(promise2, x, resolve, reject)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
        // onRejectedFail传入到失败时的回调函数集
        this.onRejectedFail.push(() => {
          setTimeout(() => {
            try {
              var x = onRejected(this.reason)
              resolvePromise(promise2, x, resolve, reject)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
      }
    })

    return promise2
  }

  function resolvePromise(promise2, x, resolve, reject) {
    // 如果 x === promise2，则会造成循环引用
    if (x === promise2) {
      return reject(new TypeError('Chaining cycle detected for promise!'))
    }

    // 防止多次调用
    var called

    // x不是null 且x是对象或者函数
    if (x != null && (typeof x === 'object') ||
      (typeof x === 'function')) {
      try {
        // 把 x.then 赋值给 then
        var then = x.then

        // 如果then是函数，就默认是x是promise了
        if (typeof then === 'function') {
          // 如果 then 是函数，将 x 作为函数的作用域 this 运行这个then函数。
          then.call(x, y => {
            if (called) return
            called = true
            // 如果成功的回调还是pormise，就递归继续解析
            resolvePromise(promise2, y, resolve, reject)
          }, r => {
            if (called) return
            // 如果 rejectPromise 以据因 r 为参数被调用，则以据因 r 拒绝 promise
            reject(r)
          })
        } else {
          // x不是promise时
          resolve(x)
        }
      } catch (e) {
        // 如果取 x.then 的值时抛出错误 e ，则以 e 为据因拒绝 promise
        if (called) return
        called = true
        reject(e)
      }
    }
    // 如果 x 不为对象或者函数，以 x 为参数执行 promise
    else {
      resolve(x);
    }
  }

  Promise.prototype['finally'] = function (cb) {
    var P = this.constructor
    return this.then(
      v => P.resolve(cb()).then(() => v),
      r => P.resolve(cb()).then(() => { throw r })
    )
  }

  Promise.resolve = function (value) {
    return new Promise((resolve, reject) => {
      resolve(value)
    })
  }

  Promise.reject = function (reason) {
    return new Promise((resolve, reject) => {
      reject(reason)
    })
  }

  Promise.all = function (arr) {
    return new Promise((resolve, reject) => {
      var cnt = 0
      var resolved = []

      for (let i = 0; i < arr.length; i++) {
        Promise.resolve(arr[i]).then(
          value => {
            cnt++
            resolved[i] = value
            if (cnt === arr.length) {
              resolve(resolved)
            }
          },
          reason => reject(reason)
        )
      }
    })
  }

  Promise.some = function (arr) {
    return new Promise((resolve, reject) => {
      var cnt = 0
      var rejected = []

      for (let i=0; i<arr.length; i++) {
        Promise.resolve(arr[i]).then(
          value => resovle(value),
          reason => {
            cnt++
            rejected[i] = reason
            if (cnt === arr.length) {
              reject(rejected)
            }
          }
        )
      }
    })
  }

  Promise.race = function (arr) {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < arr.length; i++) {
        // 如果不是 Promise 实例，先调用Promise.resolve()方法，将参数转为 Promise 实例
        Promise.resolve(arr[i]).then(
          value => resolve(value),
          reason => reject(reason)
        )
      }
    })
  }

  return Promise
}())


Promise.defer = Promise.deferred = function () {
  let dfd = {}
  dfd.promise = new Promise((resolve, reject) => {
      dfd.resolve = resolve
      dfd.reject = reject
  })
  return dfd
}

module.exports = Promise
```

</details>

## 测试

```bash
npm i promises-aplus-tests -g

promises-aplus-tests filename.js

872 passing (20s)
```

## 参考

* [Promises/A+](https://promisesaplus.com/)
* [Promise原理与实现](https://www.jianshu.com/p/b4f0425b22a1)
* [【翻译】Promises/A+规范](https://www.ituring.com.cn/article/66566)
* [BAT前端经典面试问题：史上最最最详细的手写Promise教程](https://juejin.im/post/5b2f02cd5188252b937548ab#heading-6)
