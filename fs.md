## Node 遍历目录

### 遍历目录

同步+广度优先

广度优先的方式有点像人们正常点开文件夹时候所看到的，首先是一个文件夹下的所有文件，然后是其中所有子文件夹中的所有文件。

需要使用到一个队列。

```javascript
function wideSync(dir) {
  var dirs = [dir]
  
  while(dirs.length) {
    var curr = dirs.shift()
    console.log(curr)

    var stat = fs.statSync(curr)
    if (stat.isDirectory()) {
      var files = fs.readdirSync(curr)
  
      files.forEach(file => {
        // curr是不断变化的
        var child = path.join(curr, file)
        dirs.push(child)
      })
    }
  }
}
```

异步+广度优先

```javascript
function wide (dir, cb) {
  console.log(dir)
  // 立刻调用回调函数
  cb && cb()

  fs.readdir(dir, (err, files) => {
    (function next(idx) {
      if (idx >= files.length) return
      var child = path.join(dir, files[idx])

      fs.stat(child, (err, stat) => {
        if (stat.isDirectory()) {
          wide(child, next(idx + 1))
        }else {
          console.log(child)
          next(idx + 1)
        }
      })
    }(0))
  })
}
```

同步+深度优先

```javascript
function deepSync(dir) {
  var files = fs.readdirSync(dir)

  files.forEach(file => {
    var child = path.join(dir, file)
    var stat = fs.statSync(child)

    if (stat.isDirectory()) {
      deepSync(child)
    } else {
      console.log(child)
    }
  })
}
```

异步+深度优先

```javascript
function deep(dir, cb) {
  fs.readdir(dir, (err, files) => {
    (function next(idx){
      if (idx === files.length) {
        return cb()
      }
      var child = path.join(dir, files[idx])
      fs.stat(child, (err, stat) => {
        if (stat.isDirectory()) {
          // 先遍历下child，下一个要去的idx + 1
          // 将child内的文件遍历完之后就会调用回调函数
          deep(child, () => next(idx + 1))
        }else {
          console.log(child)
          next(i + 1)
        }
      })
    }(0))
  })
}
```

### 测试

假设目录结构如下

```utf8
/a目录下：
|-- d.txt
|-- e.txt
|-- b
|   |-- f.xls
|
|-- c
    |-- h.txt
    |-- g
        |-- j.js
        |
        |-- i
            |--k.ppt
```

对齐进行遍历操作

1、 同步 + 深度优先

```javascript
deepSync('a')
// a/b/f.xls
// a/c/g/i/k.ppt
// a/c/g/j.js
// a/c/h.txt
// a/d.txt
// a/e.txt
```

2、 同步 + 广度优先

```javascript
wideSync('a')
// a
// a/b
// a/c
// a/d.txt
// a/e.txt
// a/b/f.xls
// a/c/g
// a/c/h.txt
// a/c/g/i
// a/c/g/j.js
// a/c/g/i/k.ppt
```

3、 异步 + 深度优先

```javascript
deep('a', () => {
  console.log('-----')
})
// a/b/f.xls
// a/c/g/i/k.ppt
// a/c/g/j.js
// a/c/h.txt
// a/d.txt
// a/e.txt
// -----
```

4、 异步 + 广度优先

```javascript
wide('a')
// a
// a/b
// a/c
// a/d.txt
// a/b/f.xls
// a/e.txt
// a/c/g
// a/c/h.txt
// a/c/g/i
// a/c/g/j.js
// a/c/g/i/k.ppt
```
