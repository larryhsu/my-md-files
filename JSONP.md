## 同源策略

同源策略指的三个相同：

* 协议相同
* 域名相同
* 端口相同

举个栗子：

`https://www.larryhsu.com/articles/81`这个网址，协议是`https`，域名是`www.larryhsu.com`，端口是`443`（https默认端口是443）所以上面的网址相当于：

* `https://www.larryhsu.com:443/articles/81`

那么它的同源情况如下：

* `https://www.larryhsu.com/archives`，同源，协议域名端口均相同。

* `http://www.larryhsu.com/archives`，不同源，协议不同，这里是http。

* `https://larryhsu.com:443/articles/81`，不同源，域名不同。

* `https://www.larryhsu.com:80/articles/81`，不同源，端口不同，这里的端口是80端口。

### 同源策略的目的

浏览器同源策略是浏览器安全的基础，其目的是保护用户的隐私和信息安全，防止恶意的网站窃取数据。如果没有同源策略的话，考虑下以下这几种情况：
>如果某个黑客在网站中使用了一个iframe加载某个购物网站的登陆界面，等用户进来我这个页面的时候，初看之下并没有发现什么异常，于是在他输入账号密码的时候，该网站就能获取到用户输入的账号数据，从而给用户造成损失。而有了同源策略之后，在该网站上就不能够获取用户输入的数据了。

在非同源的情况下：

1. Cookie、LocalStorage 和 IndexDB 无法读取。
2. DOM 无法获得。
3. AJAX 请求不能发送。

## JSONP

但是同源策略也有其缺点，当我们在开发某个网站的时候，往往会采用前后端分离的方法，后端提供一个api接口给前端调用，前端获取到数据后进行展示。这个时候前后端往往是不同源的，这个时候就需要一些方法来用于解决跨域的问题，JSONP就是其中之一。其他的方法还有CORS，或者服务器代理等。

关于CORS实现跨域请看这篇文章：

* [跨域资源共享 CORS 详解](https://www.larryhsu.com/articles/55)

下面说说JSONP。

JSONP的全称是JSON with Padding，用于解决AJAX跨域问题的一种方案。

由于同源策略的限制，浏览器只允许`XmlHttpRequest`请求当前源的资源，而对请求`script`资源没有限制。通过请求`script`标签实现跨域请求，然后在服务端输出`JSON`数据并执行回调函数，这种跨域的数据的方式被称为`JSONP`。

### 实现原理

JSONP 由两部分组成：回调函数和数据。回调函数是当响应到来时应该在页面中调用的函数。回调函数的名字一般是在请求中指定的。而数据就是传入回调函数中的JSON数据。

举个例子：

前端：

```JavaScript
var myFunc = function (opt) {
  alert(opt.message)
}
var script = document.createElement("script")
script.src = "http://127.0.0.1:3000?callback=myFunc"
document.body.insertBefore(script, document.body.firstChild)
```

后端（koa）：

```javascript
router.get('/', async (ctx, next) => {
  var cb = ctx.query.callback
  ctx.body = cb + "({'message': 'hello world'})"
})
```

最后的效果是：

![跨域成功](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*Z5IHFzv*TikeFhD7xIqRqkSWEFu*muYZPDtCPl65AqeQ606iHwXv5Vrb.Ebqtfijg!!/b&bo=rgX9AAAAAAADB3Q!&rf=viewer_4)


### JOSNP优缺点

JSONP 之所以在开发人员中极为流行，主要原因是它非常简单易用。它的优点在于能够直接访问响应文本，支持在浏览器与服务器之间双向通信。不过，JSONP 也有不足之处:

1. JSONP 是从其他域中加载代码执行。如果其他域不安全，很可能会在响应中夹带一些恶意代码，而此时除了完全放弃 JSONP 调用之外，没有办法追究。因此在使用不是你自己运维的 `Web` 服务时，一定得保证它安全可靠。

2. 要确定 `JSONP` 请求是否失败并不容易。虽然 `HTML5` 给`<script>` 元素新增了一个 `onerror` 事件处理程序，但目前还没有得到任何浏览器支持。为此，开发人员不得不使用计时器检测指定时间内是否接收到了响应。但就算这样也不能尽如人意，毕竟不是每个用户上网的速度和带宽都一样。

3. JSONP的请求只能是GET请求。
