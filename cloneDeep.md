## 深拷贝的实现

首先定义一些工具函数备用，作为后面用来判断变量类型的标记。后面可以使用toString来获取准确的引用类型：

> 每一个引用类型都有`toString`方法，默认情况下，`toString()`方法被每个`Object`对象继承。如果此方法在自定义对象中未被覆盖，`toString()` 返回 "`[object type]`"，其中`type`是对象的类型。

```javascript
/**
* 可以继续遍历的类型
*/
const mapTag = "[object Map]";
const setTag = "[object Set]";
const arrayTag = "[object Array]";
const objectTag = "[object Object]";
const argsTag = "[object Arguments]";
```

```js
/**
* 不可以继续遍历的类型
*/
const boolTag = "[object Boolean]";
const dateTag = "[object Date]";
const errorTag = "[object Error]";
const numberTag = "[object Number]";
const regexpTag = "[object RegExp]";
const stringTag = "[object String]";
const symbolTag = "[object Symbol]";
const funcTag = "[object Function]";
```

```js
const deepTag = [
  mapTag, 
  setTag, 
  arrayTag, 
  objectTag, 
  argsTag
];
```

上面将能JavaScript中所能想到的类型分为可以继续遍历的类型和不可以继续遍历的类型。还需要一些工具函数：

```javascript
// 获取类型
const getType = obj => Object.prototype.toString.call(obj);
// 判断数组
const isArray = obj => Array.isArray(obj) 
  || getType(obj) === arrayTag;
// 判断Map结构
const isMap = obj => getType(obj) === mapTag;
// 判断set结构
const isSet = obj => getType(obj) === setTag;
// 判断object，因为null也是object所以要排除掉
const isObject = obj => {
  var type = typeof obj;
  return obj !== null && (type === "object" || type === "function");
}
// 获取obj的构造函数，并初始化
const getInit = function(obj) {
  const Ctor = obj.constructor;
  return new Ctor();
}
// 工具函数，通用遍历
const forEach = function(array, callback) {
  let index = -1;
  const length = array.length;
  while(++index < length) {
    callback(array[index], index);
  }
  return array;
}
```

### 克隆函数

思路：使用正则提取函数体和函数参数，然后使用Function构造函数建立一个新的函数。其实，多个对象完全可以引用同一个类型的函数，这并没有什么问题。

- 逆序肯定环视：`(?<=Expression)`，表示所在位置左侧能够匹配`Expression`
- 顺序肯定环视：`(?=Expression)`，表示所在位置右侧能够匹配`Expression`

如果传入一个箭头函数，则其没有`prototype`属性，直接使用`eval`执行该函数的字符串。

```javascript
const cloneFunciton = function(func) {
  const bodyReg = /(?<={)(.|\n)+(?=})/m;
  const paramReg = /(?<=\().+(?=\)\s+{)/;
  const funcString = func.toString();

  // 箭头函数没有prototype
  if(func.prototype) { 
    const body = bodyReg.exec(funcString);
    const param = paramReg.exec(funcString);

    if(body) {
      if(param) {
        const paramArr = param[0].split(",");
        return new Function(...paramArr, body[0]);
      } else {
        return new Function(body[0]);
      }
    } else {
      return null;
    }
  } else {
    return eval(funcString);
  }
}
```

### 克隆正则

```javascript
const cloneReg = function(obj) {
  const Ctor = obj.constructor;
  // 匹配结尾的若干个字符
  const regFlag = /\w*$/.exec(obj)[0]; 
  const result = new Ctor(obj.source, regFlag);
  result.lastIndex = obj.lastIndex;
  return result;
}
```

### 克隆Symbol

```javascript
const cloneSymbol = function (obj) {
  return Object(Symbol.prototype.valueOf.call(obj));
}
```

### 克隆不可遍历类型

```js
const cloneJustOnce = function(obj, type) {
  const Ctor = obj.constructor;
  switch(type) {
    case boolTag:
    case numberTag:
    case stringTag:
    case errorTag:
    case dateTag:
      return new Ctor(obj);
    case regexpTag:
      return cloneReg(obj);
    case symbolTag:
      return cloneSymbol(obj);
    case funcTag:
      return cloneFunciton(obj);
    default:
      return null;
  }
}
```

### 实现

深拷贝还需要解决一个循环引用的问题，解决这个问题的思路在于，可以额外开辟一个存储空间，用来存储当前对象和拷贝对象的对应关系，在拷贝当前对象的时候，先去看看存储空间中是否已经有了该对象，如果有了就放弃拷贝该对象，如果没有则再继续拷贝。

这个存储空间，需要存储`key-value`形式的数据，因此`Map`数据结构就非常合适，因此在克隆一个对象的时候：
- 检查下`Map`中是否有克隆过的对象，如果有的话直接返回
- 没有的时候，将当前的被克隆的对象作为`key`，克隆对象作为`value`存储在Map数据结构中。
- 继续下一步操作

这种存储键值对的数据结构，使用`Map`就非常不错，但是使用Map增加了对已有对象的引用。例如：

```js
const obj = { name : 'larry'}
const target = new Map();
target.set(obj, 'hsu');
obj = null;
```

虽然手动的将`obj`置空了，但是在`target`的`Map`数据结构中，还存在着对于`target`的引用。所以这部分内存依然无法被释放。而使用`WeakMap`就可以完美解决这个问题。

`WeakMap` 对象是一组键/值对的集合，其中的键是弱引用的。其键必须是对象，而值可以是任意的，因此在使用`WeakMap`的时候，`target` 和 `obj` 存在的就是弱引用关系，当下一次垃圾回收机制执行时，这块内存就会被释放掉。

```javascript
function cloneDeep(obj, map = new WeakMap()) {
  // 原始类型直接返回
  if(!isObject(obj)) {
    return obj;
  }

  const type = getType(obj);

  // 根据不同类型进行操作
  let cloneObj;
  if(deepTag.includes(type)) {
    cloneObj = getInit(obj);
  } else {
    return cloneJustOnce(obj, type);
  }

  // 处理循环引用
  if(map.get(obj)) {
    return obj;
  } 
  map.set(obj, cloneObj);

  // 处理map和set
  if(type === setTag) {
    obj.forEach(item => {
      cloneObj.add(cloneDeep(item));
    });
    return cloneObj;
  } 

  if(type === mapTag) {
    obj.forEach((value, key) => {
      cloneObj.set(key, cloneDeep(value));
    });
    return cloneObj;
  }

  // 处理对象和数组
  const keys = type === arrayTag ? undefined : Object.keys(obj);

  forEach(keys || obj, (value, key) => {
    if(keys) { // keys为对象的所有的键
      key = value;
    }
    cloneObj[key] = cloneDeep(obj[key], map);
  });
  return cloneObj;
}
```

### 源码

<details>
<summary>展开查看</summary>

```javascript
// 可以继续遍历的类型
const mapTag = "[object Map]";
const setTag = "[object Set]";
const arrayTag = "[object Array]";
const objectTag = "[object Object]";
const argsTag = "[object Arguments]";

// 不可以继续遍历的类型
const boolTag = "[object Boolean]";
const dateTag = "[object Date]";
const errorTag = "[object Error]";
const numberTag = "[object Number]";
const regexpTag = "[object RegExp]";
const stringTag = "[object String]";
const symbolTag = "[object Symbol]";
const funcTag = "[object Function]";

const deepTag = [mapTag, setTag, arrayTag, objectTag, argsTag];

const getType = obj => Object.prototype.toString.call(obj);

const isArray = obj => Array.isArray(obj) 
  || getType(obj) === arrayTag;

const isMap = obj => getType(obj) === mapTag;

const isSet = obj => getType(obj) === setTag;

const isObject = obj => {
  var type = typeof obj;
  return obj !== null && (type === "object" || type === "function");
}

// 工具函数，获取obj的构造函数，并初始化
const getInit = function(obj) {
  const Ctor = obj.constructor;
  return new Ctor();
}

// 工具函数，克隆函数
// \s 匹配一个空白字符，包括空格、水平制表符、换行符、回车符等
const cloneFunciton = function(func) {
  const bodyReg = /(?<={)(.|\n)+(?=})/m;
  const paramReg = /(?<=\().+(?=\)\s+{)/;
  const funcString = func.toString();

  // 箭头函数没有prototype
  if(func.prototype) { 
    const body = bodyReg.exec(funcString);
    const param = paramReg.exec(funcString);

    if(body) {
      if(param) {
        const paramArr = param[0].split(",");
        return new Function(...paramArr, body[0]);
      } else {
        return new Function(body[0]);
      }
    } else {
      return null;
    }
  } else {
    return eval(funcString);
  }
}

// 工具函数，克隆正则
const cloneReg = function(obj) {
  const Ctor = obj.constructor;
  const regFlag = /\w*$/.exec(obj)[0]; // 匹配结尾的若干个字符
  const result = new Ctor(obj.source, regFlag);
  result.lastIndex = obj.lastIndex;
  return result;
}

// 工具函数，克隆Symbol
const cloneSymbol = function (obj) {
  return Object(Symbol.prototype.valueOf.call(obj));
}

// 克隆不可遍历类型
const cloneJustOnce = function(obj, type) {
  const Ctor = obj.constructor;
  switch(type) {
    case boolTag:
    case numberTag:
    case stringTag:
    case errorTag:
    case dateTag:
      return new Ctor(obj);
    case regexpTag:
      return cloneReg(obj);
    case symbolTag:
      return cloneSymbol(obj);
    case funcTag:
      return cloneFunciton(obj);
    default:
      return null;
  }
}

// 工具函数，通用遍历
const forEach = function(array, callback) {
  let index = -1;
  const length = array.length;
  while(++index < length) {
    callback(array[index], index);
  }
  return array;
}

function cloneDeep(obj, map = new WeakMap()) {
  // 原始类型直接返回
  if(!isObject(obj)) {
    return obj;
  }

  const type = getType(obj);

  // 根据不同类型进行操作
  let cloneObj;
  if(deepTag.includes(type)) {
    cloneObj = getInit(obj);
  } else {
    return cloneJustOnce(obj, type);
  }

  // 处理循环引用,解决循环引用问题，我们可以额外开辟一个存储空间，
  // 来存储当前对象和拷贝对象的对应关系，当需要拷贝当前对象时，先去存储空间中找，
  // 有没有拷贝过这个对象，如果有的话直接返回，如果没有的话继续拷贝，这样就巧妙化解的循环引用的问题。
  if(map.get(obj)) {
    return obj;
  } 
  map.set(obj, cloneObj);

  // 处理map和set
  if(type === setTag) {
    obj.forEach(item => {
      cloneObj.add(cloneDeep(item));
    });
    return cloneObj;
  } 

  if(type === mapTag) {
    obj.forEach((value, key) => {
      cloneObj.set(key, cloneDeep(value));
    });
    return cloneObj;
  }

  // 处理对象和数组
  const keys = type === arrayTag ? undefined : Object.keys(obj);
 
  forEach(keys || obj, (value, key) => {
    if(keys) { // keys为对象的所有的键
      key = value;
    }
    cloneObj[key] = cloneDeep(obj[key], map);
  });
  return cloneObj;
}
```

</details>




