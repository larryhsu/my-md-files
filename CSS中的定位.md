## CSS 中的定位

### fixed

`position:fixed` 在日常的页面布局中非常常用，在许多布局中起到了关键的作用。它的作用是将元素将相对于屏幕视口（`viewport`）的位置来指定其位置。并且元素的位置在屏幕滚动时不会改变。

但是在一些情况下，`position:fixed` 将会失效。MDN 用一句话概括了这种情况：

> 当元素祖先的 transform  属性非 none 时，容器由视口改为该祖先。

```html
<html>
  <style>
    .root {
      position:absolute;
      left:50%;
      top:50%;
      width:400px;
      height:400px;
      background:red;
      transform: translate(-50%, -50%);
      display:grid;
      place-items:center;
    }
    .fixed {
      position:fixed;
      background: blue;
      width:200px;
      height:200px;
      color:red;
      display: grid;
      place-items:center;
    }
  </style>
<body>
  <div class='root'>
    <div class='fixed'>
      Fixed Element
    </div>
  </div>
</body>
</html>
```

其表现为：

![fixed](http://r.photo.store.qq.com/psc?/V11kAkef0Yp2sl/TmEUgtj9EK6.7V8ajmQrEGXpOe.LLjdiLOaAG0I*XkjDtEyw3W8RUk185UWTyGzms11jr6oVbhwkB05ZseNo2XzqiKRb.gWGtmo*6mGfvT0!/r)


### absolute

`position: absolute`生成绝对定位的元素，相对于 `static` 定位以外的第一个父元素进行定位。元素的位置通过 `left`, `top`, `right` 以及 `bottom` 属性进行规定。可通过`z-index`进行层次分级。

定位为`absolute`的元素脱离正常文本流，但与`relative`的区别是其在正常流中的位置不再存在。



### relative


### static

