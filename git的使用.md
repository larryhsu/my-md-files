# git 使用

## git 基础

每次提交更新时，它会纵览一遍所有文件的指纹信息并对文件作一快照，然后保存一个指向这次快照的索引。

在保存到 Git 之前，所有数据都要进行内容的校验和（checksum）计算，并将此结果作为数据的唯一标识和索引。Git 使用 SHA-1 算法计算数据的校验和，通过对文件的内容或目录的结构计算出一个 SHA-1 哈希值，作为指纹字符串。实际上，所有保存在 Git 数据库中的东西都是用此哈希值来作索引的，而不是靠文件名。

### 三种状态

对于任何一个文件，在 Git 内都只有三种状态：

- 已修改（modified）：表示修改了某个文件，但还没有提交保存
- 已提交（committed）：表示该文件已经被安全地保存在本地数据库中了
- 已暂存（staged）：表示把已修改的文件放在下次提交时要保存的清单中

> 每个项目都有一个 git 目录，它是 Git 用来保存元数据和对象数据库的地方。该目录非常重要，每次克隆镜像仓库的时候，实际拷贝的就是这个目录里面的数据。

基本的 Git 工作流程如下所示：

1. 在工作目录中修改某些文件
2. 对这些修改了的文件作快照，并保存到暂存区域
3. 提交更新，将保存在暂存区域的文件快照转储到 git 目录中

如果是 git 目录中保存着的特定版本文件，就属于已提交状态；如果作了修改并已放入暂存区域，就属于已暂存状态；如果自上次取出后，作了修改但还没有放到暂存区域，就是已修改状态。

### 文件状态

要确定哪些文件当前处于什么状态，可以用 git status 命令

![文件状态](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*WNC*0VmtfgAwEca3UcZpiqCJsoM6rxRhaa8iX5WmOZ0SHzFdIQd9qU0UY4GphE3QQ!!/b&bo=eQKjAXkCowEDCSw!&rf=viewer_4)

### 跟踪文件

可以使用命令 git add 开始跟踪一个新文件：例如：

```bash
git add README.md
```

### 文件差异

如果要查看具体修改了什么地方，可以用 git diff 命令，git diff 会使用文件补丁的格式显示具体添加和删除的行。

### 提交更新

每次准备提交前，先用 git status 看下，是不是都已暂存起来了，然后再运行提交命令 git commit，使用 -m 参数后跟提交说明的方式，在一行命令中提交更新：

```bash
git commit -m 'first commit'
```

Git 提供了一个跳过使用暂存区域的方式，只要在提交的时候，给 git commit 加上 -a 选项，Git 就会自动把所有已经跟踪过的文件暂存起来一并提交，从而跳过 git add 步骤。

### 移除文件

要从 Git 中移除某个文件，就必须要从暂存区域移除，然后提交。可以用 git rm 命令完成此项工作，并连带从工作目录中删除指定的文件，这样以后就不会出现在未跟踪文件清单中了。

如果删除之前修改过并且已经放到暂存区域的话，则必须要用强制删除选项 -f。

### 提交历史

1. 在提交了若干更新之后，又或者克隆了某个项目，想回顾下提交历史，可以使用 git log 命令。

```bash
$ git log
commit a2b8097fc54c3b61b9c6d92b372e09cbdb335dda (HEAD -> master, origin/master)
Author: larryhsu <larry_hsu@yeah.net>
Date:   Tue May 26 14:25:51 2020 +0800

  line-height

commit 9e4fc1ee6a0a54774aa792a20bc9ad7f275d83c0
Author: larryhsu <larry_hsu@yeah.net>
Date:   Fri May 15 16:09:15 2020 +0800

  fix bug

commit b30401b4c567f288f4b379d65e0a7cb008331ce1
Author: larryhsu <larry_hsu@yeah.net>
Date:   Fri May 15 15:39:30 2020 +0800

  add select
...
```

默认不用任何参数的话，git log 会按提交时间列出所有的更新，最近的更新排在最上面。

2. git reflog 可以查看所有分支的所有操作记录（包括已经被删除的 commit 记录和 reset 的操作）

例如执行 git reset --hard HEAD~1，退回到上一个版本，用 git log 只能看到当前版本之前的操作，用 git reflog 则可以看到之后的操作，我们就可以买后悔药，恢复到这个版本之后的版本去。

### 修改最后一次提交

有时候我们提交完了才发现漏掉了几个文件没有加，或者提交信息写错了。想要撤消刚才的提交操作，可以使用 --amend 选项重新提交。

### 取消已经暂存的文件

可以使用 `git reset HEAD <file>...` 的方式取消暂存。

```bash
$ git status
On branch feature-d

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
  modified:   test.txt

$ git reset test.txt
Unstaged changes after reset:
M       test.txt

$ git status
On branch feature-d

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
  modified:   test.txt
```

### 取消对文件的修改

使用 `git checkout -- <file>...` 抛弃文件修改

```bash
$ git checkout -- test.txt

$ git status
On branch feature-d
Your branch is up to date with 'origin/feature-d'.

nothing to commit, working tree clean
```

### 查看远程仓库

可以用 git remote 命令查看当前配置有哪些远程仓库。

```bash
$ git remote
origin

$ git remote -v
origin  https://gitee.com/larryhsu/git-turtorial.git (fetch)
origin  https://gitee.com/larryhsu/git-turtorial.git (push)
```

### 添加远程仓库

```bash
$ git remote
origin

$ git remote add pb git://github.com/paulboone/ticgit.git

$ git remote -v
origin git://github.com/schacon/ticgit.git
pb git://github.com/paulboone/ticgit.git
```

### 从远程仓库抓取数据

可以用`git fetch [remote-name]`命令从远程仓库抓取数据到本地。

如果是克隆了一个仓库，此命令会自动将远程仓库归于 origin 名下。所以，git fetch origin 会抓取从你上次克隆以来别人上传到此远程仓库中的所有更新，fetch 命令只是将远端的数据拉到本地仓库，并不自动合并到当前工作分支，只有当你确实准备好了，才能手工合并。

如果设置了某个分支用于跟踪某个远端仓库的分支（参见下节及第三章的内容），可以使
用 git pull 命令自动抓取数据下来，所以一般我们运行 git pull，目的都是要从原始克隆的远端仓库中抓取数据后，合并到工作目录中当前分支。

### 推送数据到远程仓库

可以使用`git push [remote-name] [branch-name]`。如果要把本地的 master 分支推送到 origin 服务器上（再次说明下，克隆操作会自动使用默认的
master 和 origin 名字），可以运行下面的命令：

```bash
git push origin master
```

### 查看远程仓库信息

使用命令：`git remote show [remote-name]`查看某个远程仓库的详细信息。

```bash
$ git remote show origin
* remote origin
  Fetch URL: https://gitee.com/larryhsu/git-turtorial.git
  Push  URL: https://gitee.com/larryhsu/git-turtorial.git
  HEAD branch: master
  Remote branches:
    feature-d tracked
    master    tracked
  Local branches configured for 'git pull':
    feature-d merges with remote feature-d
    master    merges with remote master
  Local refs configured for 'git push':
    feature-d pushes to feature-d (local out of date)
    master    pushes to master    (local out of date)
```

### 远程仓库的删除和重命名

可以用 git remote rename 命令修改某个远程仓库的简短名称，比如想把 pb 改成 paul，可以这么运行：

```bash
$ git remote rename pb paul
$ git remote
origin
paul
```

碰到远端仓库服务器迁移，或者原来的克隆镜像不再使用，又或者某个参与者不再贡献代码，那么需要移除对应的远端仓库，可以运行 git remote rm 命令：

```bash
$ git remote rm paul
$ git remote
origin
```

### 标签（跳过）

## 分支

当使用 git commit 新建一个提交对象前，Git 会先计算每一个子目录（本例中就是项目根目录）的校验和，然后在 Git 仓库中将这些目录保存为树（tree）对象。

Git 仓库中有五个对象：三个表示文件快照内容的 blob 对象；一个记录着目录树内容及其中各个文件对应 blob 对象索引的 tree 对象；以及一个包含指向 tree 对象的索引和其他提交信息元数据的 commit 对象。

![一次提交后仓库里的数据](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*bfqYEonu0iTtqJc4xqZJG2pAeBAEFsDFQPp8ZMK4vQQlUST3LgDURcv*YF3HyXdSA!!/b&bo=lAKbAZQCmwEDCSw!&rf=viewer_4)

作些修改后再次提交，那么这次的提交对象会包含一个指向上次提交对象的指针。

![多次提交后的 Git 对象数据](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/S1G4*2hi*D5aPIJug2nMa4HD4zM0LBKN8SZzlR9p8nxpMgEVtIb*N.L86PgbA11.NmRWXExZYYBqyd4rPWPWvdlmpsgxZpBgLllI8XuenIA!/b&bo=lwIoAZcCKAEDGTw!&rf=viewer_4)

Git 中的分支，其实本质上仅仅是个指向 commit 对象的可变指针。Git
会使用 master 作为分支的默认名字。在若干次提交后，你其实已经有了一个指向最后一次提交对象的 master 分支，它在每次提交的时候都会自动向前移动。

![指向提交数据历史的分支](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*ZDAXz4inYwUAKbsYptkrdW3NebLiqNrwW9p3U86qPpef9SdL42a3dRELf4NZXxEqw!!/b&bo=vQJSAb0CUgEDCSw!&rf=viewer_4)

可以使用命令`git branch [branch name]`创建一个分支。

```bash
git branch testing
```

Git 是如何知道你当前在哪个分支上工作的呢？其实答案也很简单，它保存着一个名为 HEAD 的特别指针，它是一个指向你正在工作中的本地分支的指针。

![HEAD指针](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*ftiDdBmpqGqoiq9LZmPflkUz1hNnkenqIpts15FERTaUfgMq1IXZ1RlFhd3AVaJDg!!/b&bo=TwKOAU8CjgEDCSw!&rf=viewer_4)

要切换到其他分支，可以执行 git checkout 命令。

```bash
git checkout testing
```

我们可以在不同分支里反复切换，并在时机成熟时把它们合并到一起。

由于 Git 中的分支实际上仅是一个包含所指对象校验和的文件，所以创建和销毁一个分支就变得非常廉价。新建一个分支就是向一个文件写入 41 个字节那么简单。

### 合并分支

假设当前状态如下：

![当前状态](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*RmmqomsXJJPq2HebaToocJv4D.RWrgUAH*yZQSiOGSnriRt.E63TbDCLOutKcvUbg!!/b&bo=GAJcARgCXAEDCSw!&rf=viewer_4)

使用 git merge 来合并：

```bash
$ git checkout master
$ git merge hotfix
```

此时会出现`fast forward`提示，如果顺着一个分支走下去可以到达另一个分支，那么 Git 在合并两者时，只会简单地把指针前移，因为没有什么分歧需要解决，所以这个过程叫做快进（Fast forward）。

现在变成了这样：

![现在变成了这样](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*f1hCrbNX.X.hD.rXlEOZVY5e7fO1SMY9.UNztDaJk*a6j7fUlZTOegEciOsUdozXg!!/b&bo=4AGYAeABmAEDCSw!&rf=viewer_4)

假设再次回到 iss53 开始工作，直到问题解决了后想与 master 分支进行合并操作，首先是解决 iss53 后的状态：

![解决iss53](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*fSqp3lBpta*Jx7COYHkkLI*HDYIWVVCkH*AT26lQnTz3KXHAN21n9.TfyX4N9D68g!!/b&bo=JwI2AScCNgEDCSw!&rf=viewer_4)

在问题 #53 相关的工作完成之后，现在想合并回 master 分支，只需检出想要更新的分支（master），并运行 git merge 命令指定来源：

```bash
git checkout master
git merge iss53
```

这次合并的实现，并不同于之前 hotfix 的并入方式。这一次，你的开发历史是从更早的地方开始分叉的。由于当前 master 分支所指向的 commit (C4)并非想要并入分支（iss53）的直接祖先，Git 不得不进行一些处理。

Git 会用两个分支的末端（C4 和 C5）和它们的共同祖先（C2）进行一次简单的三方合并计算。

![三方合并](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*eJkuz1AEhcRfLWe4RiSYID3DZMJ8ElnJUaTOIHpev3UltyplAWQv5*hP7wWqGCb2g!!/b&bo=2gIuAtoCLgIDCSw!&rf=viewer_4)

有时候合并操作并不会如此顺利。如果你修改了两个待合并分支里同一个文件的同一部分，Git 就无法干净地把两者合到一起。

如果你在解决问题 #53 的过程中修改了 hotfix 中修改的部分，将得到类似下面的结果：

```bash
$ git merge iss53
Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
Automatic merge failed; fix conflicts and then commit the result.
```

此时就需要人工来决定怎么在文件之间进行取舍了。

### 衍合

可以使用 git rebase 命令进行衍合：

```bash
get checkout testing
git rebase master
```

有了 rebase 命令，就可以把在一个分支里提交的改变在另一个分支里重放一遍。

它的原理是回到两个分支（你所在的分支和你想要衍合进去的分支）的共同祖先，提取你所在分支每次提交时产生的差异（diff），把这些差异分别保存到临时文件里，然后从当前分支转换到你需要衍合入的分支，依序施用每一个差异补丁文件。

> 衍合能产生一个更为整洁的提交历史

## git 内部原理

从根本上来讲 Git 是一套内容寻址 (content-addressable) 文件系统，在此之上提供了一个 VCS 用户界面。

如果你打开`.git`文件，会发现有如下几个文件：

```bash
HEAD
info/
objects/
refs/
hooks/
config
index
description
```

解释：

- description 文件仅供 GitWeb 程序使用，不用关心这些内容
- config 文件包含了项目特有的配置选项
- info 目录保存了一份不希望在 .gitignore 文件中管理的忽略模式 (ignored patterns) 的全局可执行文件
- hooks 目录包住了客户端或服务端钩子脚本
- objects 目录存储所有数据内容
- refs 目录存储指向数据 (分支) 的提交对象的指针
- HEAD 文件指向当前分支
- index 文件保存了暂存区域信息

Git 是一套内容寻址文件系统。从内部来看，Git 是简单的 key-value 数据存储。它允许插入任意类型的内容，并会返回一个键值，通过该键值可以在任何时候再取出该内容。

### 树对象

`Git` 以一种类似 `UNIX` 文件系统但更简单的方式来存储内容。所有内容以 `tree` 或 `blob` 对象存储，其中 tree 对象对应于 `UNIX` 中的目录，`blob` 对象则大致对应于 `inodes` 或文件内容。一个单独的 `tree` 对象包含一条或多条 `tree` 记录，每一条记录含有一个指向 `blob` 或
子 `tree` 对象的 `SHA-1` 指针，并附有该对象的权限模式 (mode)、类型和文件名信息。

Git 保存的数据如图所示:

![Git 保存的数据](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*fVw3zGUkdJBIQTMWlTtRdHc8gCL9XiZnbrL20SL1YmxlJOC5xbSltwVksjGFZFRZA!!/b&bo=9ALEAfQCxAEDCSw!&rf=viewer_4)

### commit 对象

每一个 commit 对象都指向了你创建的树对象快照。

使用

```bash
git add
git commit
```

后所进行的工作：保存修改了的文件的 blob，更新索引，创建 tree 对象，最后创建 commit 对象，这些 commit 对象指向了顶层 tree 对象以及先前的 commit 对象。这三类 Git 对象 ── blob，tree 以及 tree ── 都各自以文件的方式保存在 .git/objects 目录下。

一般而言，在进行了几次提交后，git 目录下可能如下所示：

![Git 目录下的所有对象](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*QHP*EHZ3C.TFe0OyDB1D9Chfla4EgD*GJx2jtHPU6u4MyojDPdXf1cDirpFpvpi2w!!/b&bo=iALPAYgCzwEDCSw!&rf=viewer_4)

### git reference

你可以执行像 `git log 1a410e` 这样的命令来查看完整的历史，但是这样你就要记得`1a410e`是你最后一次提交，这样才能在提交历史中找到这些对象。你需要一个文件来用一个简单的名字来记录这些 SHA-1 值，这样你就可以用这些指针而不是原来的 `SHA-1` 值去检索了。
在 `Git` 中，我们称之为`引用`（references 或者 refs）。你可以在 `.git/refs` 目录下面找到这些包含 `SHA-1` 值的文件。

加上`ref`后的目录结构可能如下所示：

![加上`ref`后的目录结构](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*fyxkUJUfpV006bYblIm7kmehHxeRI5.S9Iqplq3O4cqryA4sC1dHxBHmYnQiGwkcA!!/b&bo=igJpAYoCaQEDCSw!&rf=viewer_4)

每当你执行 `git branch [branch name]` 这样的命令，`Git` 基本上就是执行 `update-ref` 命令，把你现在所在分支中最后一次提交的 `SHA-1` 值，添加到你要创建的分支的引用。
