## 大致过程

从输入URL到展示总体来说可以分为以下几个部分：

* 重定向：浏览器重定向
* 查看缓存：有无缓存，强缓存或者协商缓存等
* DNS解析：将域名解析成 IP 地址
* TCP连接：TCP 三次握手
* 请求：发送 HTTP 请求  
* 回应：服务器处理请求并返回 HTTP 报文
* 展示：浏览器解析渲染页面
* 断开连接：TCP 四次挥手

这个问题面试官很喜欢问，因为从这个问题可以延伸出上面那么多条的内容出来。

一条一条的来分析下。

## 查看缓存

在展示一个页面时，浏览器并不会傻傻的直接跑过去向服务器发送请求，很有可能的是先要查看下缓存中有没有相应的资源，然后再根据具体的情况决定是否要向服务器发送请求，关于浏览器缓存机制，我之前写过一片文章：

* [浏览器缓存机制](https://www.larryhsu.com/articles/60)

## DNS解析

需要注意的是，DNS解析过程中，也存在查看缓存的过程，不能和上一步的查看缓存弄混了。更详细的关于DNS解析相关的过程可以查看我的另一篇文章：

* [DNS解析过程](https://www.larryhsu.com/articles/78)

## TCP链接

TCP全名为传输控制协议，在OSI七层模型中属于传输层协议。许多的应用层协议都用到了它，例如HTTP，SMTP，IMAP等。

OSI七层模型如下图所示：

![OSI七层模型](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/S1G4*2hi*D5aPIJug2nMay3hJR.AEWhjhThfXUElXBgbD*ucKDE.htHsOm6VbzLjXjEo6px16pdVdsijJqLLR8TA24yfG2N*dC0mfAJquuo!/b&bo=XwE.AV8BPgEDGTw!&rf=viewer_4)

TCP是面向连接的协议，它的最明显的特征就是在传输之前需要进行3次握手，如下如所示：

![3次握手](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/S1G4*2hi*D5aPIJug2nMa5N.jcxRAnRliszBBjP7X2WNk8cwAKr7gG6uxdkRkQGpbB0*6FaSP147aj.Zb4AF.sPI5uyPrtaU.WK8DEUpSqc!/b&bo=pQGQAaUBkAEDGTw!&rf=viewer_4)

TCP三次握手的过程：

1. 客户端向服务器发送一个数据包，服务器收到后可以确定客户端具有发送数据的能力。  
2. 服务器向客户端发回一个数据包，此时客户端收到后就能确认服务具有发送和接受数据的能力。  
3. 客户端回应服务器一个数据包，此时服务器收到数据包后就能确认客户端具有接受数据的能力，此时双方都确定的彼此都具有发送和接受数据的能力，通信就可以开始了。

所以，不多不少需要三次。建立好TCP连接后，接下来就可以进行通信了。

## 请求

TCP 三次握手结束后，开始发送 HTTP 请求报文。

HTTP 请求报文由请求行（request line）、请求头（header）、请求体几个部分组成，如下图所示：

![HTTP 请求报文](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/S1G4*2hi*D5aPIJug2nMa7HCc5viBvCmKa6WUY3J4qFbL1BYCmzyAq4H07sawqIA63f58E8kVQDCs.yr13E4NTNRyCBoAw7KhrJJIaME7zg!/b&bo=MAPuATAD7gERGS4!&rf=viewer_4)

`方法`是面向对象技术中使用的专门名词。所谓`方法`就是对所请求的对象进行的操作，因此这些方法实际上也就是一些命令。因此，请求报文的类型是由它所采用的方法决定的。

### 方法

* OPTION 请求一些选项的信息
* GET 请求读取由 URL所标志的信息
* HEAD 请求读取由 URL所标志的信息的首部
* POST 给服务器添加信息（例如，注释）
* PUT 在指明的 URL下存储一个文档
* DELETE 删除指明的 URL所标志的资源
* TRACE 用来进行环回测试的请求报文
* CONNECT 用于代理服务器

### URL

`URL`是所请求的资源的 URL。

### 版本

`版本`是 HTTP 的版本。HTTP发展到现在历经了HTTP/0.9、HTTP/1.0、HTTP1.1以及HTTP2.0等各个版本，更详细的内容可以查看这篇文章：

* [HTTP历程](https://www.larryhsu.com/articles/67)

### 首部行

请求头包含请求的附加信息，由关键字/值对组成，每行一对，关键字和值用英文冒号`:`分隔。

请求头部通知服务器有关于客户端请求的信息。它包含许多有关的客户端环境和请求正文的有用信息。其中比如：Host，表示主机名，虚拟主机；Connection,HTTP/1.1 增加的，使用 keepalive，即持久连接，一个连接可以发多个请求；User-Agent，请求发出者，兼容性以及定制化需求。

### 请求体

请求体在HTTP请求报文中用的非常少。

## 回应

HTTP响应报文如下图所示：

![http 响应报文](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/S1G4*2hi*D5aPIJug2nMa5sn6GrRLWgqze*zrKYH7pGPlLmvClOwsC4p2wXrKHIAMXeXe8RCfN4uRiOjMMj7QI.4PUaXgz7KGSkOrrv5Yfg!/b&bo=CAP2AQgD9gERGS4!&rf=viewer_4)

响应报文的开始行是状态行。状态行包括三项内容，即 HTTP 的版本，状态码，以及解释状态码的简单短语。

### 状态码

* 1xx 表示通知信息的，如请求收到了或正在进行处理。
* 2xx 表示成功，如接受或知道了。
* 3xx 表示重定向，表示要完成请求还必须采取进一步的行动。
* 4xx 表示客户的差错，如请求中有错误的语法或不能完成。
* 5xx 表示服务器的差错，如服务器失效无法完成请求。

### 响应主体

响应主体包含回车符、换行符和响应返回数据，并不是所有响应报文都有响应数据。

## 展示

展示的部分就是将HTTP请求后，得到的文件（例如CSS，JS，图片等文件）进行解析后展示在浏览器页面的正确位置上。

更具体的过程可以参考我的另一篇文章：

* [浏览器渲染过程](https://www.larryhsu.com/articles/58)

## 断开链接

当数据传送完毕，需要断开 tcp 连接，此时发起 tcp 四次挥手。

1. 浏览器告诉服务器，我请求报文发送完了，你准备关闭吧  
2. 服务器告诉浏览器，我请求报文接受完了，我准备关闭了，你也准备吧  
3. 服务器告诉浏览器，我响应报文发送完了，你准备关闭吧  
4. 浏览器告诉服务器，我响应报文接受完了，我准备关闭了，你也准备吧

### 为什么要四次呢

TCP是全双工信道，何为全双工就是客户端与服务端建立两条通道：

* 通道1:客户端的输出连接服务端的输入。
* 通道2:客户端的输入连接服务端的输出。

两个通道可以同时工作：客户端向服务端发送信号的同时服务端也可以向客户端发送信号。

所以关闭双通道的时候就是这样：

* 客户端：我要关闭输入通道了。 服务端：好的，你关闭吧，我这边也关闭这个通道。
* 服务端：我也要关闭输入通道了。 客户端：好的你关闭吧，我也把这个通道关闭。

每个通道都要两次交流，所以需要四次。

## 参考

* [从URL输入到页面展现到底发生什么？](https://juejin.im/post/5bf3ad55f265da61682afc9b)

* [(1.6w字)浏览器灵魂之问，请问你能接得住几个？](https://juejin.im/post/5df5bcea6fb9a016091def69)

* 《图解HTTP》上野 宣 著

* 《计算机网络》第六版 谢希仁编著
