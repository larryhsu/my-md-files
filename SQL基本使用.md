## 概述

### 主键

表中每一行都应该有一列（或几列）可以唯一标识自己。需要满足：

- 任意两行都不具有相同的主键值；
- 每一行都必须具有一个主键值（主键列不允许 NULL 值）；
- 主键列中的值不允许修改或更新；
- **主键值不能重用（如果某行从表中删除，它的主键不能赋给以后的新行）。**

> 主键通常定义在表的一列上，但并不是必需这么做，也可以一起使用多个列作为主键。在使用多列作为主键时，上述条件必须应用到所有列，所有列值的组合必须是唯一的（**但单个列的值可以不唯一**）。

### SQL 语句

1. 结束 SQL 语句多条 SQL 语句必须以分号（；）分隔。
2. SQL 语句不区分大小写。
3. 在处理 SQL 语句时，其中所有空格都被忽略。下面这三种写法的作用是一样的。

```SQL
SELECT prod_name
FROM Products;

SELECT prod_name FROM Products;

SELECT
prod_name
FROM
Products;
```

## 检索数据

### DISTINCT

```SQL
SELECT DISTINCT vend_id
FROM Products;
```

DISTINCT 关键字指示数据库只返回不同的值。

> **不能部分使用 DISTINCT**，DISTINCT 关键字作用于所有的列，不仅仅是跟在其后的那一列。例如，你指定 SELECTDISTINCT vend_id, prod_price，除非指定的两列完全相同，否则所有的行都会被检索出来。

### LIMIT

1. 只检索前 5 行数据。

```SQL
SELECT prod_name
FROM Products
LIMIT 5;
```

2. 任意 5 行数据

```SQL
SELECT prod_name
FROM Products
LIMIT 5 OFFSET 5;
```

`LIMIT 5 OFFSET 5` 指示 MySQL 等 DBMS 返回从第 5 行起的 5 行数据,LIMIT 指定返回的行数。带 OFFSET 的 LIMIT 指定从哪儿开始。

> 第一个被检索的行是第 0 行，而不是第 1 行。因此，LIMIT 1 OFFSET 1 会检索第 2 行，而不是第 1 行。

上述语句可以简化为

```SQL
SELECT prod_name
FROM Products
LIMIT 5, 5;
```

## 排序检索数据

### ORDER BY

1. 一个列排序

```SQL
SELECT prod_name
FROM Products
ORDER BY prod_name;
```

> 在指定一条 ORDER BY 子句时，应该保证它是 SELECT 语句中最后一条子句。如果它不是最后的子句，将会出现错误消息。

2. 多个列排序

```SQL
SELECT prod_id, prod_price, prod_name
FROM Products
ORDER BY prod_price, prod_name;
```

检索 3 个列，并按其中两个列对结果进行排序——首先按价格，然后按名称排序。仅在多个行具有相同的 prod_price 值时才对产品按 prod_name 进行排序。

3. 按列的位置排序

```SQL
SELECT prod_id, prod_price, prod_name
FROM Products
ORDER BY 2, 3;
```

ORDER BY 2，3 表示先按 prod_price，再按 prod_name 进行排
序。

4. 排序的方向

数据排序不限于升序排序（从 A 到 Z），这只是默认的排序顺序。还可以使用 ORDER BY 子句进行降序（从 Z 到 A）排序。为了进行降序排序，必须指定 DESC 关键字。

1. 按照单列

```SQL
SELECT prod_id, prod_price, prod_name
FROM Products
ORDER BY prod_price DESC;
```

2. 按照多列

```SQL
SELECT prod_id, prod_price, prod_name
FROM Products
ORDER BY prod_price DESC, prod_name;
```

DESC 关键字只应用到直接位于其前面的列名。在上例中，只对 prod_price 列指定 DESC，对 prod_name 列不指定。因此，prod_price 列以降序排序，而 prod_name 列（在每个价格内）仍然按标准的升序排序。**如果想在多个列上进行降序排序，必须对每一列指定 DESC 关键字。**

## 过滤检索数据

### WHERE

```SQL
SELECT prod_name, prod_price
FROM Products
WHERE prod_price = 3.49;
```

这条语句从 products 表中检索两个列，但不返回所有行，只返回 prod_price 值为 3.49 的行。

> 在同时使用 ORDER BY 和 WHERE 子句时，应该让 ORDER BY 位于 WHERE 之后，否则将会产生错误。

1. 列出所有价格小于 10 美元的产品

```SQL
SELECT prod_name, prod_price
FROM Products
WHERE prod_price < 10;
```

2. 列出所有不是供应商 DLL01 制造的产品

```SQL
SELECT vend_id, prod_name
FROM Products
WHERE vend_id != 'DLL01';
```

> 如果将值与字符串类型的列进行比较，就需要限定引号。用来与数值列进行比较的值不用引号。

> !=和<>通常可以互换。但是，并非所有 DBMS 都支持这两种不等于操作符。

3. 检索价格在 5 美元和 10 美元之间的所有产品

```SQL
SELECT prod_name, prod_price
FROM Products
WHERE prod_price BETWEEN 5 AND 10;
```

4. 空值检查

```SQL
SELECT prod_name
FROM Products
WHERE prod_price IS NULL;
```

返回所有没有价格（空 prod_price 字段，不是价格为 0）的产品。

## 高级数据过滤

### AND

```SQL
SELECT prod_id, prod_price, prod_name
FROM Products
WHERE vend_id = 'DLL01' AND prod_price <= 4;
```

检索由供应商 DLL01 制造且价格小于等于 4 美元的所有产品的名称和价格。

### OR

```SQL
SELECT prod_name, prod_price
FROM Products
WHERE vend_id = 'DLL01' OR vend_id = ‘BRS01’;
```

OR 操作符告诉 DBMS 匹配任一条件而不是同时匹配两个条件

> SQL（像多数语言一样）在处理 OR 操作符前，优先处理 AND 操作符。 此问题的解决方法是使用圆括号对操作符进行明确分组。

> 任何时候使用具有 AND 和 OR 操作符的 WHERE 子句，都应该使用圆括号明确地分组操作符。

### IN

```SQL
SELECT prod_name, prod_price
FROM Products
WHERE vend_id IN ( 'DLL01', 'BRS01' )
ORDER BY prod_name;
```

此 SELECT 语句检索由供应商 DLL01 和 BRS01 制造的所有产品。IN 操作符后跟由逗号分隔的合法值，这些值必须括在圆括号中。

IN 操作符完成了与 OR 相同的功能，为什么要使用 IN 操作符？其优点为：

- 在有很多合法选项时，IN 操作符的语法更清楚，更直观。
- 在与其他 AND 和 OR 操作符组合使用 IN 时，求值顺序更容易管理。
- IN 操作符一般比一组 OR 操作符执行得更快。
- IN 的最大优点是可以包含其他 SELECT 语句，能够更动态地建立 WHERE 子句。

### NOT

WHERE 子句中的 NOT 操作符有且只有一个功能，那就是否定其后所跟的任何条件。因为 NOT 从不单独使用。

```SQL
SELECT prod_name
FROM Products
WHERE NOT vend_id = 'DLL01'
ORDER BY prod_name;
```

DBMS 不是匹配 vend_id 为 DLL01，而是匹配非 DLL01 之外的所有东西。

## 利用通配符过滤

为在搜索子句中使用通配符，必须使用 LIKE 操作符。

### 百分号（%）通配符

%代表搜索模式中给定位置的 0 个、1 个或多个字符。

1.

```SQL
SELECT prod_id, prod_name
FROM Products
WHERE prod_name LIKE 'Fish%';
```

找出所有以词 Fish 起头的产品。%告诉 DBMS 接受 Fish 之后的任意字符，不管它有多少字符。

2.

```SQL
SELECT prod_id, prod_name
FROM Products
WHERE prod_name LIKE '%bean bag%';
```

搜索模式`%bean bag%`表示匹配任何位置上包含文本 bean bag 的值，不论它之前或之后出现什么字符。

3.

```SQL
SELECT prod_name
FROM Products
WHERE prod_name LIKE 'F%y';
```

找出以 F 起头、以 y 结尾的所有产品。有一种情况下把通配符放在搜索模式中间是很有用的，就是根据邮件地址的一部分来查找电子邮件，例如 WHERE email LIKE b%@forta.com。

### 下划线（\_）通配符

下划线（\_）通配符的用途与%一样，但它只匹配单个字符，而不是多个字符。

```SQL
SELECT prod_id, prod_name
FROM Products
WHERE prod_name LIKE '__ inch teddy bear';
```

这个 WHERE 子句中的搜索模式给出了后面跟有文本的两个通配符。结果只显示匹配搜索模
式的行。

### 方括号（[ ]）通配符

方括号（[]）通配符用来指定一个字符集，它必须匹配指定位置（通配符的位置）的一
个字符。

```SQL
SELECT cust_contact
FROM Customers
WHERE cust_contact LIKE '[JM]%'
ORDER BY cust_contact;
```

上面的 SQL 语句中，找出所有名字以 J 或 M 起头的联系人。

```SQL
SELECT cust_contact
FROM Customers
WHERE cust_contact LIKE '[^JM]%'
ORDER BY cust_contact;
```

上面的 SQL 语句中，匹配不以 J 或 M 起头的任意联系人名。

## 计算字段

### 拼接字段

拼接表中的多个字段

```SQL
SELECT Concat(vend_name, ' (', vend_country, ')')
AS vend_title
FROM Vendors
ORDER BY vend_name;
```

### 算术计算

```SQL
SELECT prod_id,
quantity,
item_price,
quantity*item_price AS expanded_price
FROM OrderItems
WHERE order_num = 20008;
```

expanded_price 列是一个计算字段，此计算为 quantity\*item_price。客户端应用现在可以使用这个新计算列，就像使用其他列一样。

## 数据处理函数

```SQL
SELECT vend_name, UPPER(vend_name) AS vend_name_upcase
FROM Vendors
ORDER BY vend_name;
```

UPPER()将文本转换为大写

常用的文本处理函数：

- LEFT()（或使用子字符串函数） 返回字符串左边的字符
- LENGTH()（也使用 DATALENGTH()或 LEN()） 返回字符串的长度
- LOWER()（Access 使用 LCASE()） 将字符串转换为小写
- LTRIM() 去掉字符串左边的空格
- RIGHT()（或使用子字符串函数） 返回字符串右边的字符
- RTRIM() 去掉字符串右边的空格
- SOUNDEX() 返回字符串的 SOUNDEX 值
- UPPER()（Access 使用 UCASE()） 将字符串转换为大写

常用的数值处理函数：

- ABS() 返回一个数的绝对值
- COS() 返回一个角度的余弦
- EXP() 返回一个数的指数值
- PI() 返回圆周率
- SIN() 返回一个角度的正弦
- SQRT() 返回一个数的平方根
- TAN() 返回一个角度的正切

## 汇总数据

聚集函数（aggregate function） 对某些行运行的函数，计算并返回一个值。

常用的有：

- AVG() 返回某列的平均值
- COUNT() 返回某列的行数
- MAX() 返回某列的最大值
- MIN() 返回某列的最小值
- SUM() 返回某列值之和

### AVG

AVG()通过对表中行数计数并计算其列值之和，求得该列的平均值。AVG()可用来返回所有列的平均值，也可以用来返回特定列或行的平均值。

```SQL
SELECT AVG(prod_price) AS avg_price
FROM Products;

-- avg_price
-- -------------
-- 6.823333
```

此 SELECT 语句返回值 avg_price，它包含 Products 表中所有产品的平均价格。

### COUNT

COUNT()函数进行计数。可利用 COUNT()确定表中行的数目或符合特定条件的行的数目。

COUNT()函数有两种使用方式：

- 使用 COUNT(\*)对表中行的数目进行计数，不管表列中包含的是空值（NULL）还是非
  空值。
- 使用 COUNT(column)对特定列中具有值的行进行计数，忽略 NULL 值。

```SQL
SELECT COUNT(*) AS num_cust
FROM Customers;
```

返回了 Customers 表中顾客的总数

```SQL
SELECT COUNT(cust_email) AS num_cust
FROM Customers;
```

只对具有电子邮件地址的客户计数

### MAX

MAX()返回指定列中的最大值。MAX()要求指定列名。

### MIN

MIN()的功能正好与 MAX()功能相反，它返回指定列的最小值。与 MAX()一样，MIN()要求
指定列名。

### SUM

SUM()用来返回指定列值的和（总计）。

### 聚集不同的值

```SQL
SELECT AVG(DISTINCT prod_price) AS avg_price
FROM Products
WHERE vend_id = 'DLL01';
```

使用了 DISTINCT 参数，因此平均值只考虑各个不同的价格

### 聚集组合函数

```sql
SELECT COUNT(*) AS num_items,
MIN(prod_price) AS price_min,
MAX(prod_price) AS price_max,
AVG(prod_price) AS price_avg
FROM Products;

-- num_items  price_min  price_max  price_avg
-- 9          3.4900     11.9900    6.823333
```

## 分组数据

### 创建分组

```sql
SELECT vend_id, COUNT(*) AS num_prods
FROM Products
GROUP BY vend_id;

-- vend_id num_prods
-- ------- ---------
-- BRS01   3
-- DLL01   4
-- FNG01   2
```

因为使用了 GROUP BY，就不必指定要计算和估值的每个组了。系统会自动完成。GROUP BY 子句指示 DBMS 分组数据，然后对每个组而不是整个结果集进行聚集。

在使用 GROUP BY 子句前，需要知道一些重要的规定:

- GROUP BY 子句可以包含任意数目的列，因而可以对分组进行嵌套，更细致地进行数据分组。
- 如果在 GROUP BY 子句中嵌套了分组，数据将在最后指定的分组上进行汇总。换句话说，在建立分组时，指定的所有列都一起计算（所以不能从个别的列取回数据）。
- GROUP BY 子句中列出的每一列都必须是检索列或有效的表达式（但不能是聚集函
  数）。如果在 SELECT 中使用表达式，则必须在 GROUP BY 子句中指定相同的表达式。不能使用别名。
- 大多数 SQL 实现不允许 GROUP BY 列带有长度可变的数据类型（如文本或备注型字
  段）。
- 除聚集计算语句外，SELECT 语句中的每一列都必须在 GROUP BY 子句中给出。
- 如果分组列中包含具有 NULL 值的行，则 NULL 将作为一个分组返回。如果列中有多行 NULL 值，它们将分为一组。
- GROUP BY 子句必须出现在 WHERE 子句之后，ORDER BY 子句之前。

### 过滤分组

```sql
SELECT cust_id, COUNT(*) AS orders
FROM Orders
GROUP BY cust_id
HAVING COUNT(*) >= 2;

-- cust_id     orders
-- 1000000001  2
```

这条 SELECT 语句的前三行类似于上面的语句。最后一行增加了 HAVING 子句，它过滤 COUNT(\*) >= 2（两个以上订单）的那些分组。

HAVING 和 WHERE 的差别:

这里有另一种理解方法，WHERE 在数据分组前进行过滤，HAVING 在数据分组后进行过
滤。这是一个重要的区别，WHERE 排除的行不包括在分组中。这可能会改变计算值，从
而影响 HAVING 子句中基于这些值过滤掉的分组。

```sql
SELECT vend_id, COUNT(*) AS num_prods
FROM Products
WHERE prod_price >= 4
GROUP BY vend_id
HAVING COUNT(*) >= 2;
```

列出具有两个以上产品且其价格大于等于 4 的供应商。WHERE 子句过滤所有 prod_price 至少为 4 的行，然后按 vend_id 分组数据，HAVING 子句过滤计数为 2 或 2 以上的分组。

一般在使用 GROUP BY 子句时，应该也给出 ORDER BY 子句。这是保证数据正确排序的唯一方法。千万不要仅依赖 GROUP BY 排序数据。

例如：

```sql
SELECT order_num, COUNT(*) AS items
FROM OrderItems
GROUP BY order_num
HAVING COUNT(*) >= 3
ORDER BY items, order_num;
```

### SELECT 子句顺序

```sql
-- 子句      说明
-- SELECT   要返回的列或表达式
-- FROM     从中检索数据的表
-- WHERE    行级过滤
-- GROUP BY 分组说明
-- HAVING   组级过滤
-- ORDER BY 输出排序顺序
```

## 子查询

```sql
SELECT cust_name, cust_contact
FROM Customers
WHERE cust_id IN (SELECT cust_id
FROM Order
WHERE order_num IN (SELECT order_num
FROM OrderItems
WHERE prod_id = 'RGAN01'));
```

> 在 WHERE 子句中使用子查询能够编写出功能很强且很灵活的 SQL 语句。对于能嵌套的子查询的数目没有限制，不过在实际使用时由于性能的限制，不能嵌套太多的子查询。

假如需要显示 Customers 表中每个顾客的订单总数。订单与相应的顾客 ID 存储在 Orders 表中。

```sql
SELECT cust_name,
cust_state,
(SELECT COUNT(*)
FROM Orders
WHERE Orders.cust_id = Customers.cust_id) AS orders
FROM Customers
ORDER BY cust_name;
```

## 联结

为什么要使用联结？

如果数据存储在多个表中，无法使用一条 SELECT 语句就检索出数据。联结是一种机制，用来在一条 SELECT 语句中关联表，因此称
为联结。

```sql
SELECT vend_name, prod_name, prod_price
FROM Vendors, Products
WHERE Vendors.vend_id = Products.vend_id;
```

最大的差别是所指定的两列（prod_name 和 prod_price）在一个表中，而第三列（vend_name）在另一个表中。

在一条 SELECT 语句中联结几个表时，相应的关系是在运行中构造的，在联结两个表时，实际要做的是将第一个表中的每一行与第二个表中的每一行配对。WHERE 子句作为过滤条件，只包含那些匹配给定条件（这里是联结条件）的行。没有 WHERE 子句，第一个表中的每一行将与第二个表中的每一行配对，而不管它们逻辑上是否能配在一起。

上面这种方法也叫做内联结

### 内联结

将其改造如下：

```sql
SELECT vend_name, prod_name, prod_price
FROM Vendors INNER JOIN Products
ON Vendors.vend_id = Products.vend_id;
```

在使用这种语法时，联结条件用特定的 ON 子句而不是 WHERE 子句给出。传递给 ON 的实际条件与传递给 WHERE 的相同。

### 外联结

当需要完成类似于以下的工作时：

- 对每个顾客下的订单进行计数，包括那些至今尚未下订单的顾客；
- 列出所有产品以及订购数量，包括没有人订购的产品；
- 计算平均销售规模，包括那些至今尚未下订单的顾客。

就有可能用到外联结。

```sql
SELECT Customers.cust_id, Orders.order_num
FROM Customers LEFT OUTER JOIN Orders
ON Customers.cust_id = Orders.cust_id;
```

在使用 OUTER JOIN 语法时，必须使用 RIGHT 或 LEFT 关键字指定包括其所有行的表（RIGHT 指出的是 OUTER JOIN 右边的表，而 LEFT 指出的是 OUTER JOIN 左边的表）

## 组合查询

可用 UNION 操作符来组合数条 SQL 查询。利用 UNION，可给出多条 SELECT 语句，将它们的结果组合成一个结果集。

## 插入数据

把数据插入表中的最简单方法是使用基本的 INSERT 语法，它要求指定表名和插入到新行中的值。

```sql
INSERT INTO Customers
VALUES('1000000006',
  'Toy Land',
  '123 Any Street',
  'New York',
  'NY',
  '11111',
  'USA',
  NULL,
  NULL);
```

这种 SQL 语句高度依赖于表中列的定义次序，还依赖于其容易获得的次序信息。即使可以得到这种次序信息，也不能保证各列在下一次表结构变动后保持完全相同的次序。因此，编写依赖于特定列次序的 SQL 语句是很不安全的，这样做迟早会出问题。

因此，如下的 sql 语句更为安全：

```sql
INSERT INTO Customers(cust_id,
  cust_name,
  cust_address,
  cust_city,
  cust_state,
  cust_zip,
  cust_country,
  cust_contact,
  cust_email)
VALUES('1000000006',
  'Toy Land',
  '123 Any Street',
  'New York',
  'NY',
  '11111',
  'USA',
  NULL,
  NULL);
```

### 插入部分行

```sql
INSERT INTO Customers(cust_id,
  cust_name,
  cust_address,
  cust_city,
  cust_state,
  cust_zip,
  cust_country)
VALUES('1000000006',
  'Toy Land',
  '123 Any Street',
  'New York',
  'NY',
  '11111',
  'USA');
```

> 警告：如果表的定义允许，则可以在 INSERT 操作中省略某些列。省略的列必须满足以下某个条件。该列定义为允许 NULL 值（无值或空值）。在表定义中给出默认值。这表示如果不给出值，将使用默认值。如果对表中不允许 NULL 值且没有默认值的列不给出值，DBMS 将产生错误消息，并且相应的行插入不成功。

## 更新和删除数据

更新（修改）表中的数据，可以使用 UPDATE 语句。有两种使用 UPDATE 的方式：

- 更新表中的特定行；
- 更新表中的所有行。

基本的 UPDATE 语句由三部分组成，分别是：

- 要更新的表；
- 列名和它们的新值；
- 确定要更新哪些行的过滤条件。

```sql
UPDATE Customers
SET cust_email = 'kim@thetoystore.com'
WHERE cust_id = '1000000005';
```

要删除某个列的值，可设置它为 NULL（假如表定义允许 NULL 值）。如下进行：

```sql
UPDATE Customers
SET cust_email = NULL
WHERE cust_id = '1000000005';
```

从一个表中删除（去掉）数据，使用 DELETE 语句。有两种使用 DELETE 的方式：

- 从表中删除特定的行；
- 从表中删除所有行。

例如：

```sql
DELETE FROM Customers
WHERE cust_id = '1000000006';
```

DELETE 不需要列名或通配符。DELETE 删除整行而不是删除列。要删除指定的列，请使用 UPDATE 语句。

## 创建和操纵表

### 创建表

利用 CREATE TABLE 创建表，必须给出下列信息：

- 新表的名字，在关键字 CREATE TABLE 之后给出；
- 表列的名字和定义，用逗号分隔；
- 有的 DBMS 还要求指定表的位置。

每个表列要么是 NULL 列，要么是 NOT NULL 列，这种状态在创建时由表的定义规定。

```sql
CREATE TABLE Orders
(
order_num INTEGER NOT NULL,
order_date DATETIME NOT NULL,
cust_id CHAR(10) NOT NULL
);
```

SQL 允许指定默认值，在插入行时如果不给出值，DBMS 将自动采用默认值。默认值在 CREATE TABLE 语句的列定义中用关键字 DEFAULT 指定。

```sql
CREATE TABLE OrderItems
(
order_num INTEGER NOT NULL,
order_item INTEGER NOT NULL,
prod_id CHAR(10) NOT NULL,
quantity INTEGER NOT NULL DEFAULT 1,
item_price DECIMAL(8,2) NOT NULL
);
```

### 更新表

使用 ALTER TABLE 更改表结构，必须给出下面的信息：

- 在 ALTER TABLE 之后给出要更改的表名（该表必须存在，否则将出错）；
- 列出要做哪些更改。

例如：

```sql
ALTER TABLE Vendors
ADD vend_phone CHAR(20);
```

### 删除表

删除表（删除整个表而不是其内容）非常简单，使用 DROP TABLE 语句即可：

```sql
DROP TABLE CustCopy;
```

## 视图

视图是虚拟的表。与包含数据的表不一样，视图只包含使用时动态检索数据的查询。

视图用 CREATE VIEW 语句来创建。与 CREATE TABLE 一样，CREATE VIEW 只能用于创建不存在的视图。极大地简化了复杂 SQL 语句的使用。利用视图，可一次性编写基础的 SQL，然后根据需要多次使用。

例如：

```sql
CREATE VIEW ProductCustomers AS
SELECT cust_name, cust_contact, prod_id
FROM Customers, Orders, OrderItems
WHERE Customers.cust_id = Orders.cust_id
AND OrderItems.order_num = Orders.order_num;
```

## 存储过程

存储过程就是为以后使用而保存的一条或多条 SQL 语句。可将其视为批文件，虽然它们的作用不仅限于批处理。

执行存储过程的 SQL 语句很简单，即 EXECUTE。EXECUTE 接受存储过程名和需要传递给它的任何参数。

## 事务处理

## 游标

## 高级 SQL 特性
