## XSS攻击

### 同源策略

在了解xss攻击之前，先来了解下同源策略:

浏览器的同源策略是浏览器的基本功能之一，也是最核心的功能之一。协议相同，域名相同，端口相同这三大要素为浏览器的同源策略。 受到同源策略限制后：

- 本域运行的脚本只能读写本域内的资源，而无法访问其它域的资源。  
- 无法读取不同源的 `Cookie`、`LocalStorage` 和 `IndexDB` 。  
- 无法获得不同源的`DOM` 。  
- 不能向不同源的服务器发送`ajax`请求。

在浏览器中，`<script>`，`<img>`，`<iframe>`，`<link>`等标签都可以跨域加载资源，而不受同源策略的限制。这些带`src`属性的标签每次加载时，实际上是由浏览器发起一次`GET`请求，不同于`XMLHttpRequest`的是，通过`src`加载的资源，浏览器限制了`JS`的权限，使其不能读、写返回的内容。

`XMLHttpRequest`受到同源限制，不能跨域访问资源。这是因为可能会导致一些敏感数据的泄漏，此时AJAX想要跨域可以采用以下几种方法：

- JSONP  
- CORS  
- 服务器代理


### xss是什么

跨站脚本攻击，英文全称是`Cross Site Script`，本来缩写是`CSS`，但是为了和层叠样式表`Cas-cading Style Sheet，CSS`有所区别，所以在安全领域叫做`XSS`。

`XSS`攻击，通常指黑客通过`HTML`注入篡改了网页，插入了恶意的脚本，从而在用户浏览网页时，控制用户浏览器的一种攻击。 本质是一种`HTML`注入，黑客输入的数据被当成了`HTML`代码一部分来执行，从而混淆了原本的语义，产生了新的语义。


### XSS的类型

1) 反射型XSS

通过url链接点击触发，是一次性行为，实质是服务器端没有对用户的恶意输入做安全处理，直接反射相应内容，这类攻击可以通过url辨别，谷歌这类安全性高的浏览器，基本可以自动处理这类攻击。例如：

```js
const express = require('express');
const app = express();
app.get('/', function(req, res){
  res.send('hello world')
})

app.get('/index', function(req, res){
  const id = req.query.id
  res.send(`<textarea>${id}</textarea>`)
})
app.listen(3001)
```

在浏览器输入网址 

```js
localhost:3001/index?id=</textarea><script>alert(1)</script>
```

在浏览器中就能看到一个`alert`的大框框，不过`chrome`对这种简单的`xss`已经做了处理。

2) 存储型xss攻击

顾名思义，存储，一般涉及后端数据存储，常见的场景就是`APP`的意见反馈模块，前端通过接口把用户输入的信息传给后端，当有些后台管理系统需要展示反馈意见时，就会取数据库中的这些数据，当这些数据中含有攻击的脚本，那就造成了存储型`xss`攻击。这种攻击是持久性的。

3) dom型攻击

通过修改页面的DOM节点形成的XSS，称之为DOM Based XSS。例如：

```html
<html>
  <style>
    .root {
      position:absolute;
      width:510px;
      height:160px;
      left:50%;
      top:50%;
      transform: translate(-50%, -50%);
    }

    input {
      height: 50px;
      width:500px;
      display:block;
      margin:20px 0;
    }

    h1 {
      text-align: center;
    }
  </style>
<body>
  <div class='root'>
    <h1 id="t"></h1>
    <input type='text' id='text' value='https://larryhsu.com' />
    <input type='button' id='s' value='write' onclick='test()'/> 
  </div>
  <script> 
    function test() {
      var str = document.getElementById("text").value;
      document.getElementById("t").innerHTML = 
        `<a href='${str}'> testLink </a>`; 
    } 
  </script>
</body>
</html>
```

![demo](http://r.photo.store.qq.com/psc?/V11kAkef0Yp2sl/TmEUgtj9EK6.7V8ajmQrEMUhZUeW4W6Mdfuo1lG.e*oMdIrSwufVG3J4xMkHCJTLNivJdNjnunn9ejCdQTOIAUTJCiGuDNnkgpOMgh2CQQ8!/r)


点击write按钮后，会在当前页面插入一个超链接，其地址为文本框中的内容。write按钮的onclick事件调用了test函数，而在test函数中，修改了页面的dom节点，通过innerHTML把一段用户数据当成了HTML写入到页面中，这就造成了DOM based XSS。


### XSS防御


1）html标签转义

将标签符号`<`, `>`全局转义:

```js
const escapeHtml = (str) => {
  str = str.replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quto;')
    .replace(/'/g, '&#39;')
    .replace(/ /g, '&#32;')
  return str
}

app.get('/index', (req, res) => {
  const id = req.query.id || ''
  res.send(`<textarea>${escapeHtml(id)}</textarea>`)
})
```

2） 白名单过滤

高度定制要展示的内容，来将html字符串解析成html。

```js
// <img src="123" onerror="alert(1)"></img>使用cheerio的输出
{ 
  type: 'tag',
  name: 'img',
  attribs: { src: '123', onerror: 'alert(1)' },
  children: [],
  next: null,
  prev: null,
  parent: null,
  root:
   { type: 'root',
     name: 'root',
     attribs: {},
     children: [ [Circular] ],
     next: null,
     prev: null,
     parent: null 
   } 
}
```

```js
// server.js
const whiteList = {
  'img': ['src']
}

const xssFilter = (html) => {
  if (!html) return '';
  const $ = cheerio.load(html, {
    normalizeWhitespace: true,
    xmlMode: true,
  });
  $('*').each((index, elem) => {
    if (!whiteList[elem.name]) {
      $(elem).remove();
      return;
    }
    for (var attr in elem.attribs) {
      if (whiteList[elem.name].indexOf(attr) === -1) {
        $(elem).attr(attr, null);
      }
    }
  });
  onsole.log(html, $.html());
  return $.html();
}

app.get('/api', (req, res) => {
  const cb = (result) => {
    result = result.map(item => {
      const { comment, name, time } = item
      return {
        name,
        time,
        comment: xssFilter(comment),
      }
    });
    res.send({
      data: result,
      message: 'success',
      status: 1,
    });
  }
  model.find({}, cb); // 自定义的查询方法
});
```

过滤结果： 因为白名单只配置了：`img` : ['src']， 因此`img`的`onerror`属性被过滤，其他不在白名单的标签整体被过滤


## 参考

- [浅谈前端安全-xss](https://juejin.im/post/6844903950693449742)
- 《白帽子讲web安全》