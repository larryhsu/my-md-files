# 模拟实现new和bind

## new

### 说明

new 运算符创建一个用户定义的对象类型的实例或具有构造函数的内置对象的实例。new 关键字会进行如下的操作：

1. 创建一个空的简单JavaScript对象（即{}）；
2. 链接该对象（即设置该对象的构造函数）到另一个对象；
3. 将步骤1新创建的对象作为this的上下文；
4. 如果该函数没有返回对象，那么 new 表达式中的函数调用会自动返回这个新对象。

关于第四点：

```javascript
function foo(){
    this.a = 1;
    // 函数有返回对象
    return {};
}

foo.prototype.say = function(){
    console.log(this.a);
}

var x = new foo();
console.log(x); // Object {}，且x并没有say这个函数
Object.getPrototypeOf(x) === foo.prototype;  // false
```

### 语法

```javascript
new constructor[([arguments])]
```

### 参数

* constructor一个指定对象实例的类型的类或函数。

* arguments一个用于被 constructor 调用的参数列表。

### 实现

new 是关键词，不可以直接覆盖。这里使用 newOpt 来模拟实现 new 的效果。

```javascript
function newOpt (ctx) {
  if (typeof ctx !== 'function') {
    throw TypeError('the first param must be function!');
  }

  // 取出剩余参数
  var args = [...arguments].slice(1)

  // 1.创建一个空的简单JavaScript对象（即{}）；
  var obj = {}

  // 2.链接该对象（即设置该对象的构造函数）到另一个对象
  obj.constructor = ctx

  // 3.将步骤1新创建的对象作为this的上下文；
  var res = ctx.apply(obj, args)
  var isObj = typeof res === 'object' && res !== null;
  var isFun = typeof res === 'function';

  // 如果该函数有返回对象，则返回这个对象。
  if (isObj || isFun) {
    return res
  }

  // 4. 如果该函数没有返回对象，那么 new 表达式中的函数调用会自动返回这个新对象。
  return obj
}
```

## bind

提到bind方法，估计大家还会想到call方法、apply方法；它们都是Function对象内建的方法，它们的第一个参数都是用来更改调用方法中this的指向。需要注意的是bind 是返回新的函数，以便稍后调用；apply 、call 则是立即调用原函数。

### 说明

> `bind()` 方法创建一个新的函数，在 bind() 被调用时，这个新函数的 this 被指定为 bind() 的第一个参数，而其余参数将作为新函数的参数，供调用时使用。

### 语法

```javascript
func.bind(thisArg[, arg1[, arg2[, ...]]])
```

### 参数

* `thisArg`：调用绑定函数时作为 `this` 参数传递给目标函数的值。 如果使用new运算符构造绑定函数，则忽略该值。当使用 `bind` 在 `setTimeout` 中创建一个函数（作为回调提供）时，作为 `thisArg` 传递的任何原始值都将转换为 `object`。如果 `bind` 函数的参数列表为空，执行作用域的 `this` 将被视为新函数的 `thisArg`。

* `arg1, arg2...`：当目标函数被调用时，被预置入绑定函数的参数列表中的参数。

### 返回值

* 返回一个原函数的拷贝，并拥有指定的 this 值和初始参数。

### 描述

`bind()` 函数会创建一个新的绑定函数 `bound function，BF`。绑定函数是一个 `exotic function object`（怪异函数对象，ECMAScript 2015 中的术语），它包装了原函数对象。调用绑定函数通常会导致执行包装函数。

`bind`方法主要有四种特性：

1. 可以指定this
2. 返回一个函数
3. 可以传入参数
4. 柯里化

> 注：绑定函数也可以使用 `new` 运算符构造，它会表现为目标函数已经被构建完毕了似的。提供的 `this` 值会被忽略，但前置参数仍会提供给模拟函数。

### 实现

```javascript
const isFunction = function (obj) {
  return typeof obj === 'function'
}

Function.prototype.bindFn = function (thisArg) {
  // 如果 `IsCallable(func)` 是 `false`, 则抛出一个 `TypeError` 异常。
  if (!isFunction(this)) {
    throw new TypeError(`${this} is not a function`)
  }

  var _this = this

  var args = [...arguments].slice(1)

  var fNOP = function(){}

  var bound = function () {
    // 这时的arguments是指bind返回的函数传入的参数
    var bindArgs = [...arguments]
    // 注1
    return _this.apply(
      this instanceof fNOP ? this : thisArg,
      args.concat(bindArgs)
    )
  }

  // 注2
  fNOP.prototype = this.prototype
  bound.prototype = new fNOP()

  // 返回一个函数
  return bound
}
```

注1：

1. 当作为构造函数时，`this` 指向实例，此时 `this instanceof fNOP` 结果为 `true`，可以让实例获得来自绑定函数的值。
2. 当作为普通函数时，`this` 指向 `window`，此时结果为 `false`，将绑定函数的 `this` 指向 `thisArg`。

注2：

之所以不直接这样做：`bound.prototype = this.prototype`是由于直接修改 `bound.prototype` 的时候，也会直接修改 `this.prototype`。

使用了

```javascript
var fNOP = function(){}
fNOP.prototype = this.prototype
bound.prototype = new fNOP()
```

这样的解决方案后，直接修改 `bound.prototype` 的时候，不会修改`fNOP.prototype`，也不会修改 `this.prototype`, 此时需要修改`bound.prototype.__proto__`时才会修改到`fNOP.prototype`。

例如：

```javascript
function Person (name) {
  this.name = name;
}

Person.prototype.sayname = function () {
  console.log(this.name);
}

function Bound (name) {
  this.name = name;
}

Bound.prototype = Person.prototype

var larry = new Bound('larry');

larry.__proto__.sayname = function () {
  console.log('test')
}
larry.sayname(); // test
/* 此时
Person.prototype.sayname和Bound.prototype.sayname
也变为了
function () {
  console.log('test')
}
*/
```

而如果这样做的话：

```javascript
function Person () {}

Person.prototype.sayname = function () {
  console.log(this.name);
}

function Bound (name) {
  this.name = name;
}

function fNOP () {}

fNOP.prototype = Person.prototype;

Bound.prototype = new fNOP()

var larry = new Bound('larry');

larry.sayname(); // larry

larry.__proto__.sayname = function () {
  console.log('test')
}
larry.sayname(); // test

/* 此时
Person.prototype.sayname 没有变化
而Bound.prototype.sayname变为了
function () {
  console.log('test')
}

而此时还想要修改Person.prototype.sayname
要怎么办呢？
需要这样写：
larry.__proto__.__proto__.sayname = ...
*/
```

此时就不会影响到原来的对象的prototype属性。
