## CommonJS Module 

`CommonJS` 是由 `JavaScript` 社区于2009年提出的包含模块、文件、IO、控制台在内的一系列标准。在 `Nodejs` 的实现中采用了 `CommonJS` 标准的一部分，并在其基础上进行了一些调整。我们所说的 `CommonJS` 模块和 `Node.js` 中的实现并不完全一样，现在一般谈到 `CommonJS` 其实就是 `Node.js` 中的版本，而非它的原始定义。

### 模块

`CommonJS` 中规定每个文件是一个模块。将一个 `JavaScript` 文件直接通过 `script` 标签插入页面中与封装成 `CommonJS` 模块最大的不同在于，前者的顶层作用域是全局作用域，在进行变量及函数声明时会污染全局环境;而后者会形成一个属于模块自身的作用域，所有的变量及函数只有自己能访问，对外是不可见的。请看下面的例子：

### 导出

在`CommonJS`中，通过`module.exports`可以导出模块中的内容，例如：

```js
module.exports = {
  name: 'larryhsu',
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}
```

`CommonJS`模块内部会有一个`module`对象用于存放当前模块的信息，可以简单理解为在每个模块的最开始处这样定义了：

```js
var module = {...}
module.exports = {...}
```

`CommonJS` 还支持直接使用`exports`的导出方式:

```js
exports.name = 'larryhsu'
exports.getBlog = () => {
  return 'https://www.larryhsu.com'
}
```

这和使用`module.exports`并没有什么不同，其内部会将`exports`指向`module.exports`，可以简单的理解为，`CommonJS`在每个模块首部添加了以下代码：

```js
var module = {
  exports:  {}
}

var exports = module.exports
```

>注意：不能直接给`exports`赋值，否则会导致其失效。因为将`exports`指向一个新的对象断开了和`module.exports`的关系

### 导入

在`CommonJS`中使用`require`进行模块的导入。例如：

```js
// getBlog.js
module.exports = {
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}

// index.js
const getBlog = require('./getBlog.js').getBlog
const blogAddress = getBlog()
console.log(blogAddress)
// https://www.larryhsu.com
```

在`index.js`中导入了`getBlog.js`模块，并调用了其`getBlog`函数。

在require一个模块的时候，会有两种情况：

- `require` 的模块是第一次加载，这时候会首先执行这个模块，然后导出其内容
- `require` 的模块曾经被加载过，这时候该模块的代码并不会执行，而是直接导出上一次执行后得到的结果

例如：

```js
// getBlog.js
console.log('running getBlog.js')
module.exports = {
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}

// index.js
require('getBlog.js')
require('getBlog.js')

// 输出 running getBlog.js
```

在 `index.js` 中引入了两次 `getBlog.js` ，执行 `index.js` 却发现只会输出一行 `running getBlog.js`。其内部的逻辑是 `module` 对象中有一个 `loaded` 属性来记录该模块是否被加载过。默认值为 `false` ，第一次加载这个模块后该值会变为 `true` ，后面检测到这个值为 `true` 后就不会再次执行这个模块的代码了。

## ES6 Module

在 `JavaScript` 之父 `Brendan Eich` 最初设计这门语言时,原本并没有包含模块的福念。基于越来越多的工程需求,为了使用模块化进行开发, `JavaScript` 社区中涌现出了多种模块标准,其中也包括 `CommonJS` 。一直到2015年6月,由TC39标准委员会正式发布了`ES6(ECMAScript6 6.0)`,从此 `JavaScript` 语言才具备了模块这一特性。

### 模块

将上面的 `getblog.js` 和 `index.js` 使用 `ES6` 的方式改写后如下：

```js
// getBlog.js
export default {
  name: 'larryhsu',
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}

// index.js
import larry from './getBlog.js'
const blogAddress = larry.getBlog()
console.log(blogAddress)
// https://www.larryhsu.com
```

`ES6 Module` 也是将每个文件作为一个模块,每个模块拥有自身的作用域,不同的是导入、导出语句。 `Import` 和 `export` 也作为保留关键字在ES6版本中加入了进来。

`ES6 Module` 会自动采用严格模式，在 `ES6 Module` 中不管开头是否有 `use strict`，都会采用严格模式。如果将原本是`CommonJS` 的模块或任何未开启严格模式的代码改写为 `ES6 Module` 时要注意这点。

### 导出

在 `ES6 Module` 中使用 `export` 命令来导出模块。 `export` 有两种形式：

- 命名导出
- 默认导出

一个模块可以有多个命名导出。有两种不同的写法：

```js
// one
export const name = 'larryhsu'
export const getBlog = () => {
  return 'https://www.larryhsu.com'
}

// two
const name = 'larryhsu'
const getBlog = () => {
  return 'https://www.larryhsu.com'
}

export { name, getBlog }
```

第一种方式变量的声明和导出写在一行，第二种则是分开写。这两种方式的效果是一样的。

使用命名导出时，可以通过 `as` 关键字对变量进行重命名。

```js
const name = 'larryhsu'
const getBlog = () => {
  return 'https://www.larryhsu.com'
}
export { name, getBlog as getBlogAddress }
// 在导入使用时就是name和getBlogAddress了
```

而模块的默认导出只能有一个。

```js
export default {
  name: 'larryhsu'
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}
```

可以将 `export default` 理解为对外输出了一个名字为 `default` 的变量，因此不需要进行变量声明，直接导出值就行了。

### 导入

`ES6` 使用 `import` 关键字进行导入模块，对于命名导出的模块：

```js
// getBlog.js
const name = 'larryhsu'
const getBlog = () => {
  return 'https://www.larryhsu.com'
}
export { name, getBlog }

// index.js
import { name, getBlog } from './getBlog.js'
getBlog()
```

加载带有命名导出的模块时, `import` 后面要跟一对大括号来将导入的变量名包裹起来，并且这些变量名需要与导出的变量名完全一致。导入变量的效果相当于在当前作用域下声明了这些变量，并且不可对其进行更改，也就是所有导入的变量都是只读的。

与命名导出类似，可以通过 `as` 关键字可以对导人的变量重命名。如

```js
import { name, getBlog as get } from './getBlog.js'
get()
// https://www.larryhsu.com
```

在导入多个变量时，还可以采用整体导入的方式：

```js
import * as larry from './getBlog.js'
console.log(larry.name)
console.log(larry.getBlog())
```

使用 `import as <myModule>` 可以把所有导入的变量作为属性值添加到 `<myModule>` 对象中，从而减少对当前作用域的影响。

默认导出与命名导出的使用方式则有些不一样：

```js
// getBlog.js
export default {
  name: 'larryhsu',
  getBlog: () => {
    return 'https://www.larryhsu.com'
  }
}

// index.js
import larry from './getBlog.js'
larry.getBlog()
// https://www.larryhsu.com
```

对于默认导出来说， `import` 后面直接跟变量名，并且这个名字可以自由指定，其指代了 `getBlog.js` 中默认导出的值，可以这样来理解：

```js
import { default as larry } from './getBlog.js'
```

需要注意的是，这两种方式是可以一起使用的。

```js
import React, { Component } from 'react'
```

这里的 `React` 对应的是该模块的默认导出，而 `Component` 则是其命名导出的一个变量。

## CJS和ESM的区别

### 输出缓存和输出引用

（1）`CJS` 输出的是值的缓存

```js
// a.js
let a = 1;
let b = { num: 1 }

setTimeout(() => {
  a = 2;
  b = { num: 2 };
}, 200);

module.exports = {
  a,
  b,
};

// main.js
let { a, b } = require('./a');
console.log(a);  // 1
console.log(b);  // { num: 1 }

setTimeout(() => {
  console.log(a);  // 1
  console.log(b);  // { num: 1 }
}, 500);
```

可以理解为 `CJS` 输出的是原始值的一份缓存，原始值变了也不会影响到之前引入进来的值。其实是因为 `exports` 对象是模块内外的唯一关联， `commonjs` 输出的内容，就是 `exports` 对象的属性，模块运行结束，属性就确定了。


（2）`ESM` 输出的是值的引用

```js
// a.js
let a = 1;
let b = { num: 1 }

setTimeout(() => {
  a = 2;
  b = { num: 2 };
}, 200);

export {
  a,
  b,
};

// main.js
import { a, b } from './a';

console.log(a);  // 1
console.log(b);  // { num: 1 }

setTimeout(() => {
  console.log(a);  // 2
  console.log(b);  // { num: 2 }
}, 500);
```


这是 `ESM` 输出引用跟 `CJS` 输出值的区别，模块内部引用的变化，会反应在外部，这是 `ESM` 的规范。



### 循环依赖

模块A依赖模块`B`，模块`B`又依赖模块`A`，互相依赖产生了死循环。所以各个模块方案本身设计了一套规则来解决这个问题。在循环依赖的情况下，模块会出现执行中断，然后我们可以看到 `ESM` 提升和 `CJS` 的区别。

```js
// a.js
console.log('a.js 开始执行');
exports.done = false;
let b = require('./b');
console.log('b.done: ', b.done);  // true
exports.done = true;
console.log('a.js执行完毕');

// b.js
console.log('b.js 开始执行')
exports.done = false;
let a = require('./a');
console.log('a.done: ', a.done);  // false
exports.done = true;
console.log('b.js执行完毕');

// main.js
console.log('main.js 开始执行')
let a = require('./a');  // 在a.js文件中依赖了b.js文件
let b = require('./b');  // 在b.js文件中依赖了a.js文件
console.log('main.js: ', a.done, b.done);
```

执行 `node main.js` 后的输出结果：

```
1. a.js 开始执行
2. b.js 开始执行
3. a.done:  false
4. b.js执行完毕
5. b.done:  true
6. a.js执行完毕
7. main.js:  true true
```

这个例子能提现 `commonjs` 运行时加载的情况。因为 `a.js` 依赖 `b.js`，`b.js` 又依赖 `a.js`，所以当 `b.js` 执行到`require('./a')`的时候，`a.js` 会暂停执行，所以此时`require('./a')`返回的是`false`，但是在`main.js`中，`a.js`的返回值又是`true`，所以这说明了 `commonjs` 模块的 `exports` 是动态执行的，具体 `require` 能获取到的值，取决于模块的运行情况。


而在`ESM`中时：

```js
// a.mjs
export let a_done = false;
import { b_done } from './b.mjs';
console.log('a.js: b.done: ', b_done);
console.log('a.js执行完毕');

// b.mjs
import { a_done } from './a.mjs';
console.log('b.js: a.done: ', a_done);
export let b_done = true;
console.log('b.js执行完毕');

// main.mjs
import { a_done } from './a.mjs';
import { b_done } from './b.mjs';
console.log('main.js: ', a_done, b_done);
```

`node main.mjs` 后输出的结果

```js
ReferenceError: Cannot access 'a_done' before initialization
```

`ReferenceError`（引用错误） 对象代表当一个不存在的变量被引用时发生的错误。当你尝试引用一个未被定义的变量时，将会抛出一个 `ReferenceError`错误。

`a.js` 加载 `b.js`，而 `b.js` 又加载 `a.js`，这就形成了循环依赖。循环依赖产生时，`a.mjs` 中断执行，这时在 `b.mjs` 中`a_done`的值是什么呢？这就要考虑到 `a.mjs` 的 `import/export` 提升的问题，`a.mjs` 中的`export a_done`被提升到顶部，执行到`import './b'`时，执行权限移交到 `b.mjs`，此时`a_done`只是一个指定导出的接口，但是未定义，所以出现引用报错。


## 总结

总体来说，ESM和CJS有三个重大的差异：

- CommonJS 模块输出的是一个值的拷贝，ES6 模块输出的是值的引用。
- CommonJS 模块是运行时加载，ES6 模块是编译时输出接口。
- CommonJS 模块的require()是同步加载模块，ES6 模块的import命令是异步加载，有一个独立的模块依赖的解析阶段。


## 参考

- [commonjs 与 esm 的区别](https://juejin.im/post/6844903861166014478)










