## 异步任务的类型

在深入了解JS的异步任务以前，最好先熟悉下JS 的 Event Loop，这是是传送门：

* [Event Loop](https://www.larryhsu.com/articles/72)

### Task

task（又称之为宏任务），可以理解是每次执行栈执行的代码就是一个宏任务（包括每次从事件队列中获取一个事件回调并放到执行栈中执行）。

浏览器为了能够使得JS内部(macro)task与DOM任务能够有序的执行，会在一个(macro)task执行结束后，在下一个(macro)task 执行开始前，对页面进行重新渲染，流程如下：

```javascript
task -> render -> task
```

Task所包含的API有：

* setTimeout
* setInterval
* setImmediate(Node.js 环境)
* I/O
* UI Render

### Microtask

microtask（又称为微任务），可以理解是在当前 task 执行结束后立即执行的任务。也就是说，在当前task任务后，下一个task之前，在渲染之前。

所以它的响应速度相比setTimeout（setTimeout是task）会更快，因为无需等渲染。也就是说，在某一个macrotask执行完后，就会将在它执行期间产生的所有microtask都执行完毕（在渲染前）。

Microtask所包含的API有：

* Promise.then
* MutaionObserver
* process.nextTick(Node.js 环境)

### 运行过程

在事件循环中，每进行一次循环操作称为 tick，每一次 tick 的任务处理模型是比较复杂的，但关键步骤如下：

* 执行一个宏任务（栈中没有就从事件队列中获取）
* 执行过程中如果遇到微任务，就将它添加到微任务的任务队列中
* 宏任务执行完毕后，立即执行当前微任务队列中的所有微任务（依次执行）
* 当前宏任务执行完毕，开始检查渲染，然后GUI线程接管渲染
* 渲染完毕后，JS线程继续接管，开始下一个宏任务（从事件队列中获取）

如图所示：

![执行流程图](http://m.qpic.cn/psc?/V11kAkef0Yp2sl/j5BRZUlgKbUG5yYXn162*Zw3TwWaZ.wN.d5L3kVeCuM8keIfxovZl08FRXfDvB1wtFOQFI1xFoGHwykJQHkCZg!!/b&bo=fgHHAn4BxwIRCT4!&rf=viewer_4)

### Promise

需要注意的是，**Promise本身是同步的立即执行函数**，当在executor中执行resolve或者reject的时候, 此时是异步操作，会先执行then/catch等，当主栈完成后，才会去调用resolve/reject中存放的方法执行。

一个栗子：

```javascript
console.log('start')

new Promise((resolve) => {
  console.log('pro1')
  resolve()
  console.log('pro1 end')
})

setTimeout(() => {
  console.log('timeout')
})

console.log('end')
// 输出: start->pro1->pro1 end->end->timeout
```

当JS主线程执行遇到Promise时，

* promise.then() 的回调就是一个 task
* promise 是 resolved或rejected: 那这个 task 就会放入当前事件循环回合的 microtask queue
* promise 是 pending: 这个 task 就会放入 事件循环的未来的某个(可能下一个)回合的 microtask queue 中
* setTimeout 的回调也是个 task ，它会被放入 macrotask queue 即使是 0ms 的情况

### async

在async函数中出现await之前的代码也是同步执行的，那么在await之后的代码怎么办呢。

async和await本质上就是promise+generator的语法糖，所以await后面的代码是microtask。

举例来说：

```javascript
async function async1() {
  console.log('async1 start')
  await async2()
  console.log('async1 end')
}
async function async2() {
  console.log('async2')
}

async1()
```

其就相当于：

```javascript
async function async1() {
  console.log('async1 start')
  Promise.resolve(async2()).then(() => {
    console.log('async1 end')
  })
}
async function async2() {
  console.log('async2')
}
```

这样一看await就是一个让出线程的标志。await后面的表达式会先执行一遍，将await后面的代码加入到microtask中，然后就会跳出整个async函数来执行后面的代码。

### 测试

```javascript
async function a1 () {
  console.log('a1 start')
  await a2()
  console.log('a1 end')
}

async function a2 () {
  console.log('a2')
}

console.log('start')

setTimeout(() => {
  console.log('setTimeout')
}, 0)

Promise.resolve().then(() => {
  console.log('pro1')
})

a1()

var pro2 = new Promise((resolve) => {
  resolve('pro2.then')
  console.log('pro2')
})

pro2.then((res) => {
  console.log(res)
  Promise.resolve().then(() => {
      console.log('pro3')
  })
})
console.log('end')
```

可以先改成如下：

```javascript
async function a1 () {
  console.log('a1 start')
  Promise.resolve(a2()).then(()=>{
    console.log('a1 end')
  })
}

async function a2 () {
  console.log('a2')
}

console.log('start')

setTimeout(() => {
  console.log('timeout')
}, 0)

Promise.resolve().then(() => {
  console.log('pro1')
})

a1()

var pro2 = new Promise((resolve) => {
  resolve('pro2.then')
  console.log('pro2')
})

pro2.then((res) => {
  console.log(res)
  Promise.resolve().then(() => {
    console.log('pro3')
  })
})
console.log('end')
```

为了方便理解，这里可以假设有两个队列数组，分别为：

```javascript
var task = []
var micro = []
```

首先执行同步的任务：

```javascript
console.log('start') // 1、输出start
```

接下来执行：

```javascript
setTimeout(() => {
  console.log('timeout')
}, 0)
```

在0ms后，task队列中就有了一个任务，但不会立刻执行，为了方便解释，简写成如下

```javascript
task = ["console.log('timeout')"]
```

下面执行：

```javascript
Promise.resolve().then(() => {
  console.log('pro1')
})
```

那么micro队列中就有了这么一个任务：

```javascript
micro = ["console.log('pro1')"]
```

接下来执行a1，

```javascript
async function a1 () {
  console.log('a1 start')  // 2、输出a1 start
  Promise.resolve(a2()).then(()=>{
    console.log('a1 end')
  })
}
```

所有的代码都会被执行，同步的代码立刻执行，异步的代码将其放入相应的队列中去。因为Promise.resolve(a2())也是一个同步的代码，所以其中的a2函数也会同步的执行：

```javascript
async function a2 () {
  console.log('a2')  // 3、输出a2
}
```

而Promise.resolve().then()里面的任务是一个微任务，所以将其加入micro队列中去，此时micro队列变成：

```javascript
micro = ["console.log('pro1')", "console.log('a1 end')"]
```

接下来继续执行：

```javascript
var pro2 = new Promise((resolve) => {
  resolve('pro2.then')
  console.log('pro2')  // 4、输出pro2
```

接着

```javascript
pro2.then((res) => {
  console.log(res)
  Promise.resolve().then(() => {
    console.log('pro3')
  })
})
```

有两个then，都是异步执行的，那么micro就变成了：

```javascript
micro = ["console.log('pro1')", "console.log('a1 end')", "console.log(res)", " console.log('pro3')"]
```

ok，pro2.then执行完毕，接着执行：

```javascript
console.log('end') // 5、输出end
```

现在主线程执行完毕了，那么是时候看看micro与task中都有什么在里面了，此时的两个队列分别是：

```javascript
micro = ["console.log('pro1')", "console.log('a1 end')", "console.log(res)", " console.log('pro3')"]

task = ["console.log('timeout')"]
```

先执行micro中的代码，再执行task中的代码，那么此时一个一个执行过去就可以得到最终的执行结果

```javascript
1、start
2、a1 start
3、a2
4、pro2
5、end
6、pro1
7、a1 end
8、pro2.then
9、pro3
10、timeout
```
